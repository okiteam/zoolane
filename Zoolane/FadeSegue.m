//
//  FadeSegue.m
//  Zoolane
//
//  Created by Developer IMX on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FadeSegue.h"
#import <QuartzCore/QuartzCore.h>

@implementation FadeSegue


- (void) perform
{
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5 ;
    transition.type = kCATransitionFade;
    
    [[self.sourceViewController navigationController].view.layer addAnimation:transition forKey:kCATransition];
    [[self.sourceViewController navigationController] pushViewController:[self destinationViewController] animated:NO];
}

@end
