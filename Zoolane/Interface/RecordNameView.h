

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol RecordNameDelegate

- (void)recordNameClickedOK:(NSString*)name;
- (void)recordNameClickedClose;

@end


@interface RecordNameView : UIView{
    ApplicationController *controller;
    SoftwareModel *model;
    AudioPlayer *audioPlayer;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
@property (nonatomic,assign) id <RecordNameDelegate> delegate;

- (IBAction)textExit:(id)sender;
- (IBAction)closePressed:(id)sender;
-(void) initializeView:(NSMutableDictionary*)selectedTrack;
-(void) playSound;


@end
