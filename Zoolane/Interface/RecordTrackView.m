//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "RecordTrackView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "Page.h"
#import "NSAttributedString+Attributes.h"

@implementation RecordTrackView
@synthesize imageTrackBackground;
@synthesize delegate;
@synthesize labelTitle,buttonTrack1,buttonTrack2,buttonTrack3;
@synthesize buttonClose,buttonStoryConversations,buttonStoryNarration,buttonStoryWhole,viewStory,viewTrack,labelStoryTitle;
@synthesize newStoryEnabled,buttonNewStory;


-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordTrack_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordTrack_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);
    }
    buttonClose.exclusiveTouch = buttonNewStory.exclusiveTouch = buttonStoryConversations.exclusiveTouch = buttonStoryNarration.exclusiveTouch = buttonStoryWhole.exclusiveTouch = buttonTrack1.exclusiveTouch = buttonTrack2.exclusiveTouch = buttonTrack3.exclusiveTouch = YES;
    viewTrackOriginalFrame=self.viewTrack.frame;

    return self;
}

-(void) initializeView{
    
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    
    
    self.viewStory.hidden=TRUE;
    self.viewTrack.hidden=FALSE;

    chooseDeleteStory=FALSE;
    self.viewTrack.userInteractionEnabled=TRUE;
    
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_CHOOSE_STORY"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    
    
    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"RECORD_TRACK_CAPTION"]];
   
    
    if(INTERFACE_IS_PAD){
        [self.labelStoryTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [buttonTrack1.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonTrack2.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonTrack3.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonNewStory.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonStoryConversations.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonStoryNarration.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonStoryWhole.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"¿"]];
        [self.buttonClose setFrame:CGRectMake(679, 69, buttonClose.frame.size.width, buttonClose.frame.size.height)];
        
    }
    else {
        [self.labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE_IPHONE]];
        [buttonTrack1.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonTrack2.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonTrack3.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonNewStory.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonStoryConversations.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonStoryNarration.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonStoryWhole.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"¿"]];

    }
    [self.buttonTrack1 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonTrack2 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonTrack3 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonNewStory setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonStoryWhole setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonStoryNarration setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonStoryConversations setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];

    
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];
    labelTitle.attributedText=string;
    [labelTitle setTextAlignment:UITextAlignmentCenter];    
    labelTitle.centerVertically=TRUE;

    string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"RECORD_TYPE_CAPTION"]];
    if(INTERFACE_IS_PAD){
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"¿"]];
    }
    else{
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"¿"]];
    }
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];
    labelStoryTitle.attributedText=string;
    [labelStoryTitle setTextAlignment:UITextAlignmentCenter];
    labelStoryTitle.centerVertically=TRUE;


    self.buttonTrack1.selected=self.buttonTrack2.selected=self.buttonTrack3.selected=FALSE;
    [buttonTrack1 setTitle:[LanguageModel key2String:@"TRACK_ONE"] forState:UIControlStateNormal];
    [buttonTrack2 setTitle:[LanguageModel key2String:@"TRACK_TWO"] forState:UIControlStateNormal];
    [buttonTrack3 setTitle:[LanguageModel key2String:@"TRACK_THREE"] forState:UIControlStateNormal];
    [buttonNewStory setTitle:[LanguageModel key2String:@"RECORD_NEW_STORY"] forState:UIControlStateNormal];

    [buttonStoryWhole setTitle:[LanguageModel key2String:@"RECORD_WHOLE_TRACK"] forState:UIControlStateNormal];
    [buttonStoryNarration setTitle:[LanguageModel key2String:@"RECORD_NARRATION_TRACK"] forState:UIControlStateNormal];
    [buttonStoryConversations setTitle:[LanguageModel key2String:@"RECORD_CONVERSATION_TRACK"] forState:UIControlStateNormal];

    
    
    NSString *track1Title=[[controller.settings.records objectForKey:@"Track01"] objectForKey:@"Title"];
    NSString *track2Title=[[controller.settings.records objectForKey:@"Track02"] objectForKey:@"Title"];
    NSString *track3Title=[[controller.settings.records objectForKey:@"Track03"] objectForKey:@"Title"];
    
    if(![track1Title isEqualToString:@""])
        [buttonTrack1 setTitle:track1Title forState:UIControlStateNormal];
    
    if(![track2Title isEqualToString:@""])
        [buttonTrack2 setTitle:track2Title forState:UIControlStateNormal];
    
    if(![track3Title isEqualToString:@""])
        [buttonTrack3 setTitle:track3Title forState:UIControlStateNormal];
    
    if([controller allTracksRecorded]){
        buttonNewStory.hidden=FALSE;
        if(INTERFACE_IS_PAD)
            self.viewTrack.frame=CGRectMake(320,80,388,452);
        else{
            self.viewTrack.frame=CGRectMake(105,15,269,209);
        }
        self.imageTrackBackground.image=[UIImage imageNamed:@"record_popup_04.png"];
    }
}

-(void) playSound{
    [audioPlayer play];
}

- (IBAction)buttonHighlight:(id)sender {
    UIButton *button=(UIButton*)sender;
    [button setHighlighted:TRUE];
    [button setSelected:TRUE];
}

-(void) initializeView2{
    
    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"RECORD_TRACK_CAPTION"]];
   
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];
  

    if(INTERFACE_IS_PAD){
        [self.viewTrack setFrame:CGRectMake(93, 80, self.viewTrack.frame.size.width, self.viewTrack.frame.size.height)];
        [self.viewStory setFrame:CGRectMake(547, 80, self.viewStory.frame.size.width, self.viewStory.frame.size.height)];
        [self.buttonClose setFrame:CGRectMake(906, 70, self.buttonClose.frame.size.width, self.buttonClose.frame.size.height)];
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"¿"]];
    }
    else{
        self.viewStory.hidden=FALSE;
        self.viewTrack.hidden=TRUE;
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TRACK_CAPTION"] rangeOfString:@"¿"]];
   }

    labelTitle.attributedText=string;
    [labelTitle setTextAlignment:UITextAlignmentCenter];
    labelTitle.centerVertically=TRUE;
    self.viewStory.hidden=FALSE;
    self.viewStory.userInteractionEnabled=FALSE;
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHAT_RECORD"]] stringByAppendingString:@".aif"];
        
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    [audioPlayer play];
    audioPlayer.delegate2=self;
    self.viewTrack.userInteractionEnabled=FALSE;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(update:) userInfo:nil repeats:YES];

}

-(void)update:(NSTimer*)timer{
    if(animateButton)
        animateButton.selected=!animateButton.selected;
    //NO user interaction enabled while playing
   // self.viewStory.userInteractionEnabled=TRUE;

}


-(void) onSoundComplete:(NSURL *)url{
    if(animateButton==buttonStoryConversations){
        animateButton=nil;
        [timer invalidate];
        self.viewStory.userInteractionEnabled=TRUE;
    }
    NSString *audioFile=model.pathSource;
    buttonStoryConversations.selected=buttonStoryNarration.selected=buttonStoryWhole.selected=FALSE;
    
    if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_WHAT_RECORD"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHOLE_STORY"]] stringByAppendingString:@".aif"];
        animateButton=buttonStoryWhole;
    }
    else if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_WHOLE_STORY"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_NARRATION"]] stringByAppendingString:@".aif"];
        animateButton=buttonStoryNarration;
    }
    else if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_NARRATION"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_DIALOGUES"]] stringByAppendingString:@".aif"];
        animateButton=buttonStoryConversations;
    }
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
        
    
    [audioPlayer play];

 
}

#pragma mark Actions
- (IBAction)pressedTrack1:(id)sender {
    selectedTrack=1;
    if(chooseDeleteStory)    {
        [buttonTrack1 setSelected:TRUE];
        [self initializeView2];
    }
    else{
        [buttonTrack1 setSelected:TRUE];
        [self.delegate trackSelectClicked:1];
    }
}

- (IBAction)pressedTrack2:(id)sender {
    selectedTrack=2;
    if(chooseDeleteStory)
    {
        [buttonTrack2 setSelected:TRUE];
        [self initializeView2];
    }
    else{
        [buttonTrack2 setSelected:TRUE];
        [self.delegate trackSelectClicked:2];
    }

}
- (IBAction)pressedTrack3:(id)sender {
    selectedTrack=3;
    if(chooseDeleteStory)
    {
        [buttonTrack3 setSelected:TRUE];
        [self initializeView2];
    }
    else{
        [buttonTrack3 setSelected:TRUE];
        [self.delegate trackSelectClicked:3];
    }

}


- (IBAction)closePressed:(id)sender {
    if(timer)
        [timer invalidate];
    if(chooseDeleteStory){
        [self initializeView];
        chooseDeleteStory=FALSE;
        [self playSound];
    }
    else{
        [self.delegate trackSelectClose];
    }
}

- (IBAction)pressedNewStory:(id)sender {
    
    NSString *audioFile;
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHAT_DELETE"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    
    //labelTitle.text=[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"];
    
    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"]];
    if(INTERFACE_IS_PAD){
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"] rangeOfString:@"¿"]];
    }
    else{
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"] rangeOfString:@"¿"]];
    }
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];
    labelTitle.attributedText=string;
    [labelTitle setTextAlignment:UITextAlignmentCenter];


    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    [audioPlayer play];
    buttonNewStory.hidden=TRUE;
    self.viewTrack.frame=viewTrackOriginalFrame;
   // self.buttonClose.frame
    chooseDeleteStory=TRUE;

    
}

- (IBAction)pressedStoryWhole:(id)sender {
    NSLog(@"Selected Track: %d",selectedTrack);
    [self.delegate trackSelectRecordTypeClicked:selectedTrack:0];
}

- (IBAction)pressedStoryNarration:(id)sender {
        NSLog(@"Selected Track: %d",selectedTrack);
    [self.delegate trackSelectRecordTypeClicked:selectedTrack:1];

}

- (IBAction)pressedStoryConversation:(id)sender {
        NSLog(@"Selected Track: %d",selectedTrack);
    [self.delegate trackSelectRecordTypeClicked:selectedTrack:2];

}


@end
