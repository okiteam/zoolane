

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol SplashDelegate

- (void)splashClose;

@end


@interface SplashView : UIView{
    SoftwareModel *model;
    ApplicationController *controller;
    NSTimer *timer;

}

@property (nonatomic,assign) id <SplashDelegate> delegate;

-(void) initializeView;


@end
