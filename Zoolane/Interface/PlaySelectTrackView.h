

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "OHAttributedLabel.h"

@protocol TrackSelectControllerDelegate
- (void)trackSelectClicked:(int) trackNumber:(BOOL)removeExisting;
- (void)trackSelectClose;
@end


@interface PlaySelectTrackView : UIView<UITextFieldDelegate>{
    AudioPlayer *audioPlayer;
    ApplicationController *controller;
    SoftwareModel *model;

}

@property (weak, nonatomic) IBOutlet OHAttributedLabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack2;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack3;
@property (weak, nonatomic) IBOutlet UIButton *buttonOriginal;

@property (nonatomic,assign) id <TrackSelectControllerDelegate> delegate;

- (IBAction)pressedTrack1:(id)sender;
- (IBAction)pressedTrack2:(id)sender;
- (IBAction)pressedTrack3:(id)sender;
- (IBAction)closePressed:(id)sender;
- (IBAction)pressedOriginal:(id)sender;


-(void) initializeView;
-(void) playSound;



@end
