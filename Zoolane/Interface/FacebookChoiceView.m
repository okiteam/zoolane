//
//  FacebookChoiceView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "FacebookChoiceView.h"
#import "SoftwareModel.h"

@implementation FacebookChoiceView
@synthesize facebookChoiceDelegate;
@synthesize shareButton;


-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"FacebookChoice_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"FacebookChoice_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);
        
        for(UIView* vi in self.subviews)
        {
            if([vi isKindOfClass:[UIButton class]])
            {
                UIButton* b = (UIButton*)vi;
                b.exclusiveTouch = YES;
            }
        }
        
        NSString* lang;
        if([[LanguageModel localeId] isEqualToString:@"en"])
            lang = @"en_US";
        else if([[LanguageModel localeId] isEqualToString:@"fr"])
            lang = @"fr_FR";
        else if([[LanguageModel localeId] isEqualToString:@"es"])
            lang = @"es_ES";
        else if([[LanguageModel localeId] isEqualToString:@"br"])
            lang = @"pt_BR";
        
            
        CGRect likeFrame = INTERFACE_IS_PAD ? CGRectMake(568, 374, 90, 30) : CGRectMake(260, 147, 90, 30);
        buttonLikeView = [[FBLikeButton alloc] initWithFrame:likeFrame andUrl:@"http://www.facebook.com/pages/64-Zoo-Lane/27247233738" andLang:lang];
        [self addSubview:buttonLikeView];
        
        NSInteger facebookLogoWidth = INTERFACE_IS_PAD ? 44 : 30;
        CGRect shareLabelFrame = CGRectMake(facebookLogoWidth, 0, shareButton.frame.size.width - facebookLogoWidth, shareButton.frame.size.height - 3);
        UILabel* shareLabel = [[UILabel alloc] initWithFrame:shareLabelFrame];
        shareLabel.backgroundColor = [UIColor clearColor];
        shareLabel.text = [LanguageModel key2String:@"SHARE"];
        shareLabel.textAlignment = NSTextAlignmentCenter;
        CGFloat fontSize = INTERFACE_IS_PAD ? FONT_SIZE_MEDIUM : FONT_SIZE_IPHONE;
        shareLabel.font = [UIFont fontWithName:FONT_FUTURA size:fontSize];
        shareLabel.textColor = [UIColor whiteColor];
        [shareButton addSubview:shareLabel];
    }
    return self;
}


- (IBAction)buttonPressed:(id)sender;
{
    UIButton* but = (UIButton*)sender;
    NSInteger buttonId = but.tag;
    [facebookChoiceDelegate facebookButtonPressed:buttonId];
}


@end
