

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol ChapterDelegate

- (void)chapterClicked:(int) chapterIndex;
- (void)chapterClose;

@end


@interface ChapterSelectView : UIView<UITextFieldDelegate,UIScrollViewDelegate>{
    SoftwareModel *model;
    ApplicationController *controller;
    int selectedIndex;

}

@property (nonatomic,assign) id <ChapterDelegate> delegate;

@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *carousel;


- (IBAction)closePressed:(id)sender;
- (IBAction)scrollLeft:(id)sender;
- (IBAction)scrollRight:(id)sender;


-(void) initializeView;


@end
