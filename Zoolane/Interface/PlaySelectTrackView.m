//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "PlaySelectTrackView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

@implementation PlaySelectTrackView
@synthesize delegate;
@synthesize labelTitle;
@synthesize buttonOriginal;
@synthesize buttonTrack1;
@synthesize buttonTrack2;
@synthesize buttonTrack3;

-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self)
    {
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }

        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"PlaySelectTrack_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"PlaySelectTrack_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);

        buttonOriginal.exclusiveTouch = buttonTrack1.exclusiveTouch = buttonTrack2.exclusiveTouch = buttonTrack3.exclusiveTouch = YES;
    }
    return self;
}

-(void) initializeView{
    
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    

    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHAT_LISTEN"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    
    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"PLAY_TRACK_CAPTION"]];

    NSLog(@"%@",labelTitle.text);

    if(INTERFACE_IS_PAD){
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [buttonTrack1.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonTrack2.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonTrack3.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonOriginal.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_POPUP_VERSION]];
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"PLAY_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"PLAY_TRACK_CAPTION"] rangeOfString:@"¿"]];
    }
    else {
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [buttonTrack1.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonTrack2.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonTrack3.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonOriginal.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"PLAY_TRACK_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"PLAY_TRACK_CAPTION"] rangeOfString:@"¿"]];

    }
    
    [self.buttonTrack1 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonTrack2 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonTrack3 setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonOriginal setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];
   
    labelTitle.attributedText = string;
    [labelTitle setTextAlignment:UITextAlignmentCenter];
    labelTitle.centerVertically=TRUE;
    
    
    [buttonTrack1 setTitle:[LanguageModel key2String:@"TRACK_ONE"] forState:UIControlStateNormal];
    [buttonTrack2 setTitle:[LanguageModel key2String:@"TRACK_TWO"] forState:UIControlStateNormal];
    [buttonTrack3 setTitle:[LanguageModel key2String:@"TRACK_THREE"] forState:UIControlStateNormal];
    
    [buttonOriginal setTitle:[LanguageModel key2String:@"ORIGINAL_TRACK"] forState:UIControlStateNormal];
    
    NSString *track1Title=[[controller.settings.records objectForKey:@"Track01"] objectForKey:@"Title"];
    NSString *track2Title=[[controller.settings.records objectForKey:@"Track02"] objectForKey:@"Title"];
    NSString *track3Title=[[controller.settings.records objectForKey:@"Track03"] objectForKey:@"Title"];
    
    if(![track1Title isEqualToString:@""])
        [buttonTrack1 setTitle:track1Title forState:UIControlStateNormal];
    
    if(![track2Title isEqualToString:@""])
        [buttonTrack2 setTitle:track2Title forState:UIControlStateNormal];
    
    if(![track3Title isEqualToString:@""])
        [buttonTrack3 setTitle:track3Title forState:UIControlStateNormal];
     
    
}

-(void) playSound{
    [audioPlayer play];
}
#pragma mark Actions
- (IBAction)pressedTrack1:(id)sender
{
    [self.delegate trackSelectClicked:1:FALSE];
}

- (IBAction)pressedTrack2:(id)sender
{
    [self.delegate trackSelectClicked:2:FALSE];
}

- (IBAction)pressedTrack3:(id)sender
{
    [self.delegate trackSelectClicked:3:FALSE];
}

- (IBAction)closePressed:(id)sender
{
    [self.delegate trackSelectClose];
}

- (IBAction)pressedOriginal:(id)sender
{
    [self.delegate trackSelectClicked:0:FALSE];

}


@end
