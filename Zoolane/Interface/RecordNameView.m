//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "RecordNameView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "Page.h"

@implementation RecordNameView
@synthesize delegate;
@synthesize labelTitle,textName,buttonOK;

-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordName_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordName_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);

        for(UIView* vi in self.subviews)
        {
            if([vi isKindOfClass:[UIButton class]])
            {
                UIButton* b = (UIButton*)vi;
                b.exclusiveTouch = YES;
            }
        }
    }
    return self;
}

-(void) initializeView:(NSMutableDictionary*)selectedTrack{
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    if(INTERFACE_IS_PHONE){
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [self.textName setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonOK.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [self.buttonOK setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
        [self.textName setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];

    }
    else {
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [self.textName setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonOK.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [self.buttonOK setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
        [self.textName setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];
    }
    [self.labelTitle setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];    
    
    [buttonOK setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    labelTitle.text=[LanguageModel key2String:@"RECORD_NAME_CAPTION"];
    
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_NAME_STORY"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
   
    if(INTERFACE_IS_PHONE)
        [textName becomeFirstResponder];
    
    [buttonOK setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    labelTitle.text=[LanguageModel key2String:@"RECORD_NAME_CAPTION"];

    if(selectedTrack){
        if([[selectedTrack objectForKey:@"Path"] isEqualToString:@"Track01"])
            textName.text=[LanguageModel key2String:@"TRACK_ONE"];
        else if([[selectedTrack objectForKey:@"Path"] isEqualToString:@"Track02"])
            textName.text=[LanguageModel key2String:@"TRACK_TWO"];
        else if([[selectedTrack objectForKey:@"Path"] isEqualToString:@"Track03"])
            textName.text=[LanguageModel key2String:@"TRACK_THREE"];


    }


    
    
    
}

-(void) playSound{
    [audioPlayer play];
}

#pragma mark Actions

- (IBAction)clickedOK:(id)sender {
    if(![textName.text isEqualToString:@""]){
        [textName resignFirstResponder];
        [delegate recordNameClickedOK:textName.text];
    }
}

- (IBAction)textExit:(id)sender{
    [sender resignFirstResponder];
}
-(BOOL) canBecomeFirstResponder{
    return TRUE;
}

-(IBAction)closePressed:(id)sender{
    [delegate recordNameClickedClose];
}


@end
