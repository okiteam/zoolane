//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "ChapterSelectView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "Page.h"

@implementation ChapterSelectView
@synthesize delegate;
@synthesize carousel;

static const int THUMB_WIDTH = 150;
static const int THUMB_HEIGHT = 55;
static const int THUMB_DX = 10;
static const int THUMB_DY = 10;
static const int THUMB_WIDTH_IPHONE = 75;
static const int THUMB_HEIGHT_IPHONE = 63;
static const int THUMB_DX_IPHONE = 5;
static const int THUMB_DY_IPHONE = 10;




-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"ChapterSelect_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"ChapterSelect_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);
    }
    return self;
}

-(void) initializeView{
    
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    [self loadChapters];
    
}

#pragma mark Actions
-(void) singleTapGestureCaptured:(UITapGestureRecognizer*) gesture{

    CGPoint pt=[gesture locationInView:self.carousel];
    UIView *v=[self.carousel hitTest:pt withEvent:nil];
    if(v){
        int index=v.tag;
        [self.delegate chapterClicked:index];
    }
}
-(void) loadChapters{
    
    UITapGestureRecognizer *singletap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
     [carousel addGestureRecognizer:singletap];
    
    carousel.clipsToBounds=TRUE;
    //carousel.type=iCarouselTypeLinear;
    //carousel.frame=CGRectMake(0, 0, 200, 200);
    NSLog(@"%f %f", carousel.frame.size.width, carousel.frame.size.height);
    UIView *view;
    int i=0;
    for(i=0;i<model.book.pages.count;i++){
        Page* page=[model.book.pages objectAtIndex:i];
        UILabel *label;
        UIImageView *imageView;
        if(INTERFACE_IS_PHONE){
            view = [[UIView alloc] initWithFrame:CGRectMake(THUMB_DX_IPHONE + i * (THUMB_WIDTH_IPHONE + THUMB_DX_IPHONE), 0, THUMB_WIDTH_IPHONE, 64)] ;
            label = [[UILabel alloc] initWithFrame:CGRectMake(0,44,THUMB_WIDTH_IPHONE,20)];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,THUMB_WIDTH_IPHONE,40)] ;
            label.font = [label.font fontWithSize:FONT_SIZE];

        }
        else{
            view = [[UIView alloc] initWithFrame:CGRectMake(THUMB_DX + i * (THUMB_WIDTH + THUMB_DX), 0, THUMB_WIDTH, 110)];
            label = [[UILabel alloc] initWithFrame:CGRectMake(0,90,150,20)];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,150,90)] ;
            label.font = [label.font fontWithSize:FONT_SIZE_MEDIUM];

        }
        
        view.tag=i;

        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        [label setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];
        label.tag = 1;
        [view addSubview:label];
        imageView.tag=2;
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [view addSubview:imageView];
        NSString *thumbPath=[model.pathBook stringByAppendingPathComponent:page.thumbName];
        UIImage *thumb=[UIImage imageWithContentsOfFile:thumbPath];
        imageView.image=thumb;
        label.text=[NSString stringWithFormat:@"%d",i+1];
        [carousel     addSubview:view];
    }
    
    CGSize size = carousel.contentSize;
    if(INTERFACE_IS_PAD)
        size.width = 2 * THUMB_DX + i * (THUMB_WIDTH + THUMB_DX);
    else
        size.width = 2 * THUMB_DX_IPHONE + i * (THUMB_WIDTH_IPHONE + THUMB_DX_IPHONE);
	[carousel setContentSize:size];
    
}


- (IBAction)closePressed:(id)sender {
    [[self delegate] chapterClose];
}

- (IBAction)scrollLeft:(id)sender {
    if(INTERFACE_IS_PAD){
        if(carousel.contentOffset.x-THUMB_WIDTH-THUMB_DX>=0){
            [carousel setContentOffset:CGPointMake(    carousel.contentOffset.x-THUMB_WIDTH-THUMB_DX, 0) animated:TRUE];
        }
        NSLog(@"%f",carousel.contentOffset.x-THUMB_WIDTH - THUMB_DX);
    }
    else{
        if(carousel.contentOffset.x-THUMB_WIDTH_IPHONE-THUMB_DX_IPHONE>=0){
            [carousel setContentOffset:CGPointMake(    carousel.contentOffset.x-THUMB_WIDTH_IPHONE-THUMB_DX_IPHONE, 0) animated:TRUE];
        }
        NSLog(@"%f",carousel.contentOffset.x-THUMB_WIDTH_IPHONE - THUMB_DX_IPHONE);
    }

}

- (IBAction)scrollRight:(id)sender {
    if(INTERFACE_IS_PAD){
        if(carousel.contentOffset.x+((THUMB_WIDTH+THUMB_DX)*5)<carousel.contentSize.width){
            [carousel setContentOffset:CGPointMake(carousel.contentOffset.x+THUMB_WIDTH+THUMB_DX , 0) animated:TRUE];
        }
    }
    else{
        if(carousel.contentOffset.x+((THUMB_WIDTH_IPHONE+THUMB_DX_IPHONE)*5)<carousel.contentSize.width){
            [carousel setContentOffset:CGPointMake(carousel.contentOffset.x+THUMB_WIDTH_IPHONE+THUMB_DX_IPHONE , 0) animated:TRUE];
        }
    }

}


@end
