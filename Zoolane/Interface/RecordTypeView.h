

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "iCarousel.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

@protocol RecordTypeDelegate

- (void)recordTypeClicked:(int) recordType;
- (void)recordTypeClose;

@end


@interface RecordTypeView : UIView<AudioPlayerDelegate>{
    AudioPlayer *audioPlayer;
    SoftwareModel *model;
    ApplicationController *controller;
    NSTimer *timer;
    UIButton *animateButton;


}

@property (weak, nonatomic) IBOutlet OHAttributedLabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonStory;
@property (weak, nonatomic) IBOutlet UIButton *buttonConversation;
@property (weak, nonatomic) IBOutlet UIButton *buttonNarrations;
@property (nonatomic,assign) id <RecordTypeDelegate> delegate;

- (IBAction)pressedWholeStory:(id)sender;
- (IBAction)pressedNarrator:(id)sender;
- (IBAction)pressedDialogs:(id)sender;
- (IBAction)closePressed:(id)sender;

-(void) initializeView;
-(void) playSound;


@end
