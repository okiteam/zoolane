//
//  NewsletterView.h
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import <UIKit/UIKit.h>

@protocol NewsletterDelegate

- (void)clickedOK;

@end

@interface NewsletterView : UIView<UITextFieldDelegate>{
    NSMutableData *responseData;
}

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *labelTitle;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonRegister;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *textEmail;
@property (nonatomic,assign) id <NewsletterDelegate> delegate;

- (IBAction)textDidEndOnExit:(id)sender;
- (IBAction)facebookPressed:(id)sender;
- (IBAction)twitterPressed:(id)sender;
- (IBAction)registerPressed:(id)sender;
- (IBAction)closePressed:(id)sender;

-(void) initializeView;

@end
