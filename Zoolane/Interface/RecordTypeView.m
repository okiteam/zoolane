//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "RecordTypeView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "Page.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

@implementation RecordTypeView
@synthesize delegate;
@synthesize labelTitle,buttonConversation,buttonNarrations,buttonStory;


-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordType_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"RecordType_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);
        
        buttonConversation.multipleTouchEnabled = buttonNarrations.multipleTouchEnabled = buttonStory.multipleTouchEnabled = NO;
    }
    return self;
}

-(void) initializeView{
    
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHAT_RECORD"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    [self.buttonStory setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonNarrations setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    [self.buttonConversation setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];

    NSMutableAttributedString *string=[[NSMutableAttributedString alloc]initWithString:[LanguageModel key2String:@"RECORD_TYPE_CAPTION"]];

    if(INTERFACE_IS_PHONE){
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE_IPHONE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"¿"]];
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [buttonStory.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonNarrations.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
        [buttonConversation.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];

    }
    else {
        [string setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"?"]];
        [string setFont:[UIFont fontWithName:FONT_SERIFE size:FONT_SIZE] range:[[LanguageModel key2String:@"RECORD_TYPE_CAPTION"] rangeOfString:@"¿"]];
       
        [buttonStory.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonNarrations.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
        [buttonConversation.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
    }
    [string setPartTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] range:NSMakeRange(0, string.length)];

    
    [buttonStory setTitle:[LanguageModel key2String:@"RECORD_WHOLE_TRACK"] forState:UIControlStateNormal];
    [buttonNarrations setTitle:[LanguageModel key2String:@"RECORD_NARRATION_TRACK"] forState:UIControlStateNormal];
    [buttonConversation setTitle:[LanguageModel key2String:@"RECORD_CONVERSATION_TRACK"] forState:UIControlStateNormal];
    labelTitle.attributedText=string;
    [labelTitle setTextAlignment:UITextAlignmentCenter];
    labelTitle.centerVertically=TRUE;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(update:) userInfo:nil repeats:YES];
}

-(void) playSound{
    self.userInteractionEnabled=FALSE;

    audioPlayer.delegate2=self;
    [audioPlayer  play];
}

-(void)update:(NSTimer*)timer{
    if(animateButton)
        animateButton.selected=!animateButton.selected;
}


-(void) onSoundComplete:(NSURL *)url{
    if(animateButton==buttonConversation){
        animateButton=nil;
        [timer invalidate];
        self.userInteractionEnabled=TRUE;
    }
    NSString *audioFile=model.pathSource;
    buttonConversation.selected=buttonNarrations.selected=buttonStory.selected=FALSE;
    
    if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_WHAT_RECORD"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHOLE_STORY"]] stringByAppendingString:@".aif"];
        animateButton=buttonStory;
    }
    else if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_WHOLE_STORY"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_NARRATION"]] stringByAppendingString:@".aif"];
        animateButton=buttonNarrations;
    }
    else if([[url absoluteString] rangeOfString:[LanguageModel key2String:@"TUTORIAL_NARRATION"]].location!=NSNotFound){
        audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_DIALOGUES"]] stringByAppendingString:@".aif"];
        animateButton=buttonConversation;
    }
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    [audioPlayer play];
}


#pragma mark Actions

- (IBAction)pressedWholeStory:(id)sender {
    [delegate recordTypeClicked:0];
}

- (IBAction)pressedNarrator:(id)sender {
    [delegate recordTypeClicked:1];
    
}

- (IBAction)pressedDialogs:(id)sender {
    [delegate recordTypeClicked:2];
}

- (IBAction)closePressed:(id)sender {
    if(timer)
        [timer invalidate];
    [[self delegate] recordTypeClose];
}


@end
