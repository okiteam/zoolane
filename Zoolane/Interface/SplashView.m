//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "SplashView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "Page.h"

@implementation SplashView
@synthesize delegate;

-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        UIImage* img;
        if(INTERFACE_IS_PAD)
            img = [UIImage imageNamed:@"splash_background_ipad.png"];
        else
            img = [UIImage imageNamed:@"splash_background_iphone.png"];
        UIImageView* imView = [[UIImageView alloc] initWithImage:img];
        [self addSubview:imView];
        imView.center = CGPointMake(X_CENTER, Y_CENTER);
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:SPLASH_TIME target:self selector:@selector(update:) userInfo:nil repeats:NO];

    return self;
}

-(void) initializeView{
    
  
    
}
-(void)update:(NSTimer*)timer{
    [UIView transitionWithView:self duration:1.0f options:UIViewAnimationOptionTransitionNone animations:^(void){
        self.alpha=0.0f;
    }
    completion:^(BOOL finished){
        [self removeFromSuperview];
        [self.delegate splashClose];
    }];
 
}



@end
