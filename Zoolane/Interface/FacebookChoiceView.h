//
//  FacebookChoiceView.h
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import <UIKit/UIKit.h>
#import "FaceBookLikeButton.h"

@protocol FacebookChoiceDelegate

- (void)facebookButtonPressed:(NSInteger)buttonId;

@end


@interface FacebookChoiceView : UIView
{
    FBLikeButton* buttonLikeView;
    UIButton* shareButton;
}

@property (nonatomic,assign) id <FacebookChoiceDelegate> facebookChoiceDelegate;
@property (nonatomic) IBOutlet UIButton* shareButton;

- (IBAction)buttonPressed:(id)sender;


@end
