

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "AudioPlayer.h"
#import "OHAttributedLabel.h"

@protocol RecordTrackDelegate
- (void)trackSelectClicked:(int) trackNumber;
- (void)trackSelectClose;
- (void)trackSelectRecordTypeClicked:(int) trackNumber:(int) recordType;

@end


@interface RecordTrackView : UIView<AudioPlayerDelegate>{
    AudioPlayer *audioPlayer;
    SoftwareModel *model;
    ApplicationController *controller;
    CGRect viewTrackOriginalFrame;
    BOOL chooseDeleteStory;
    NSTimer *timer;
    float seconds;
    UIButton *animateButton;
    int selectedTrack;



}
@property bool newStoryEnabled;
@property (weak, nonatomic) IBOutlet UIImageView *imageTrackBackground;

@property (weak, nonatomic) IBOutlet UIButton *buttonNewStory;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;
@property (weak, nonatomic) IBOutlet UIView *viewTrack;
@property (weak, nonatomic) IBOutlet UIView *viewStory;
@property (weak, nonatomic) IBOutlet UIButton *buttonStoryWhole;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *labelStoryTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonStoryNarration;
@property (weak, nonatomic) IBOutlet UIButton *buttonStoryConversations;

@property (weak, nonatomic) IBOutlet OHAttributedLabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack2;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack3;

@property (nonatomic,assign) id <RecordTrackDelegate> delegate;

- (IBAction)pressedTrack1:(id)sender;
- (IBAction)pressedTrack2:(id)sender;
- (IBAction)pressedTrack3:(id)sender;
- (IBAction)closePressed:(id)sender;
- (IBAction)pressedNewStory:(id)sender;

- (IBAction)pressedStoryWhole:(id)sender;
- (IBAction)pressedStoryNarration:(id)sender;
- (IBAction)pressedStoryConversation:(id)sender;
-(void) initializeView;
-(void) initializeView2;

-(void) playSound;

- (IBAction)buttonHighlight:(id)sender;


@end
