//
//  NewsletterView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "NewsletterView.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "ApplicationController.h"

@implementation NewsletterView
@synthesize delegate;
@synthesize labelTitle;
@synthesize buttonRegister;
@synthesize textEmail;

-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"Newsletter_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"Newsletter_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);
    }
    for(UIView* v in self.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton* b = (UIButton*)v;
            b.exclusiveTouch = YES;
        }
    }
    return self;
}

-(void) initializeView{
    if(INTERFACE_IS_PAD){
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE]];
        [buttonRegister.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM]];
    }
    else {
        [self.labelTitle setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_IPHONE]];
        [buttonRegister.titleLabel setFont:[UIFont fontWithName:FONT_FUTURA size:FONT_SIZE_MEDIUM_IPHONE]];
     }

    [self.labelTitle setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];
    
    [self.labelTitle setText:[LanguageModel key2String:@"MENU_EMEIL_CAPTION"]];
    [self.buttonRegister setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    [self.buttonRegister setTitleColor:[LanguageModel UIColorFromRGB:COLOR_LABEL] forState:UIControlStateNormal];
    
     self.textEmail.delegate=self;
    
     if(INTERFACE_IS_PHONE)
         [self.textEmail becomeFirstResponder];
}

#pragma mark Actions

- (IBAction)textDidEndOnExit:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)facebookPressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[LanguageModel key2String:@"FACEBOOK_LINK"]]];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

- (IBAction)twitterPressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[LanguageModel key2String:@"TWITTER_LINK"]]];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

- (IBAction)registerPressed:(id)sender {
    
    //Validamos email
    if([ApplicationController validateEmailWithString:self.textEmail.text]){
        [self registerEmail];
        [self.delegate clickedOK];
    }
    else{
        self.textEmail.placeholder=@"Email not correct";
    }
    
}

- (IBAction)closePressed:(id)sender {
    [self.delegate clickedOK];
}

-(void) registerEmail{
    NSURL *url = [NSURL URLWithString:@"http://www.millimages.com/mobiles-app/zoolane/send_email.php"];
    
    NSString *post = [NSString stringWithFormat:@"email=%@", textEmail.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSURLConnection* theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    //we got the delegation response
    if(theConnection)
    {

        responseData = [NSMutableData data];
        NSLog(@"%@",responseData);
        NSLog(@"%@",[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    }
    else
    {
        // DLog(@"NewsLetterSubView.m sendEmail : Connection Failed");
    }
    
}


- (BOOL) canBecomeFirstResponder{
    return YES;
}


@end
