//
//  LanguageView.h
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import <UIKit/UIKit.h>

@protocol LanguageDelegate

- (void)clickedLanguage;

@end


@interface LanguageView : UIView

@property (nonatomic,assign) id <LanguageDelegate> delegate;

- (IBAction)frenchPressed:(id)sender;
- (IBAction)englishPressed:(id)sender;
- (IBAction)spanishPressed:(id)sender;
- (IBAction)brazilPressed:(id)sender;
- (IBAction)closePressed:(id)sender;

@end
