//
//  LanguageView.m
//  Zoolane
//
//  Created by Lion User on 24/09/2012.
//
//

#import "LanguageView.h"
#import "LanguageModel.h"
#import "SoftwareModel.h"

@implementation LanguageView
@synthesize delegate;


-(id) initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if(self){
        if(!INTERFACE_IS_PAD)
        {
            UIImage* img = [UIImage imageNamed:@"background_popup_iphone.png"];
            UIImageView* imgView = [[UIImageView alloc] initWithImage:img];
            [self addSubview:imgView];
        }
        
        UIView* v;
        if(INTERFACE_IS_PAD)
            v = [[[NSBundle mainBundle] loadNibNamed:@"Language_ipad" owner:self options:nil] objectAtIndex:0];
        else
            v = [[[NSBundle mainBundle] loadNibNamed:@"Language_iphone" owner:self options:nil] objectAtIndex:0];
        [self addSubview:v];
        v.center = CGPointMake(X_CENTER, Y_CENTER);

        for(UIView* vi in self.subviews)
        {
            if([vi isKindOfClass:[UIButton class]])
            {
                UIButton* b = (UIButton*)vi;
                b.exclusiveTouch = YES;
            }
        }
    }
    return self;
}


- (IBAction)frenchPressed:(id)sender {
    
    [LanguageModel setLocale:@"fr"];
    [delegate clickedLanguage];
}

- (IBAction)englishPressed:(id)sender {
    [LanguageModel setLocale:@"en"];
    [delegate clickedLanguage];
    
}

- (IBAction)spanishPressed:(id)sender {
    [LanguageModel setLocale:@"es"];
    [delegate clickedLanguage];
}

- (IBAction)brazilPressed:(id)sender {
    [LanguageModel setLocale:@"br"];
    [delegate clickedLanguage];
}

- (IBAction)closePressed:(id)sender {
   [delegate clickedLanguage];
}




@end
