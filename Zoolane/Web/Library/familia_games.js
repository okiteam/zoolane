// ┌────────────────────────────────────────────────────────────────────┐ \\
// │ DPI - JavaScript Vector Library                                 │ \\
// ├────────────────────────────────────────────────────────────────────┤ \\

// VARIABLES DO NOT TOUCH
//var svgText;
var svgDocument;
var svgNS = "http://www.w3.org/2000/svg";
var tspanArray=new Array();
var textRecords=new Array();
var scrollRecords=new Array();
var currentSentence = 0;
var timeToScrollArray = new Array();
var textNode;
var seconds=0;
var iPhone;
var iPad;
var currentHeight;
var currentKaraokeColor;
var currentTextColor;

var arrayOfTextRecords = new Array();

// CONFIG VARIABLES
var defaultStyleText;

var defaultYSpace=30;
var defaultYSpace_iphone=20;


function detectMobileDevice() {
    var ua = navigator.userAgent;
    var checker = {
    iphone: ua.match(/(iPhone|iPod)/),
    ipad: ua.match(/(iPad)/),
    blackberry: ua.match(/BlackBerry/),
    android: ua.match(/Android/)
    };
    if (checker.android){
        return true;
    }
    else if (checker.iphone){
        iPhone=true;
        return true;
    }
    else if (checker.ipad){
        iPad=true;
        return true;
    }
    else if (checker.blackberry){
        return true;
    }
    else {
        return false;
    }
}


function debug(){
    initializeText(new Array({text:'1 2 3',start:1.00, end:8.00},{text:' 4 5 6 {NEWLINE}',start:2.00, end:8.00},{text:'Lucie1-2Lucie1-2Lucie1-2Lucie1-2 {NEWLINE}',start:3.00, end:5.00},{text:'Lucie1-3',start:4.00, end:2.00},{text:'{SCROLL}',start:5.00, end:0.00},{text:'Lucie2-1  {NEWLINE}',start:7.00, end:6.00},{text:'Lucie2-2  {NEWLINE}',start:9.00, end:6.00},{text:'Lucie2-3',start:10.00, end:20.00},{text:'{SCROLL}',start:11.00, end:0.00},{text:'Lucie3',start:13.00, end:22.00},{text:'{SCROLL}',start:15.00, end:0.00},{text:'Lucie4',start:16.00, end:2.00}),1024.000000,110.000000,'FF0000','000000',1, 'fr');
    
}


function initializeText(txtRecords, width, height, karaokeColor, textColor, start, lang) {
    
    // init
    detectMobileDevice();
    if(iPhone){
        defaultStyleText="font-family: Verdana;font-size:17;stroke:#FF0000;stroke-width:0;";
        defaultYSpace=defaultYSpace_iphone;
    }
    else{
        if(lang=="fr" || lang=="en")
            defaultStyleText ="font-family:Verdana;font-size:24;stroke:#000000;stroke-width:0";
        else
            defaultStyleText ="font-family:Verdana;font-size:23;stroke:#000000;stroke-width:0";
    }
    
    currentHeight = height * 0.9;
    currentSentence = 0;
    currentKaraokeColor = karaokeColor;
    currentTextColor = textColor;
    
    svgDocument=document.getElementById("svgDocument");
    svgDocument.setAttribute("width",width);
    svgDocument.setAttribute("height",height);
    
    var i,z,j;
    z=j=0;
    textRecords=new Array();
    arrayOfTextRecords = new Array();
    timeToScrollArray = new Array();
    var currentRecords = new Array();
    
    for(i=0;i<txtRecords.length;i++){
        
        if(txtRecords[i].text=="{SCROLL}")
        {
            arrayOfTextRecords[z] = currentRecords;
            timeToScrollArray[z] = txtRecords[i].start;
            currentRecords = new Array();
            j = 0;
            z++;
        }
        else
        {
            currentRecords[j] = txtRecords[i];
            j++;
        }
    }
    // Set the last record array
    arrayOfTextRecords[z] = currentRecords;
    
    reset();
    
    if(start==1){
        createTextSpans();
        seconds = 0;
        timer();
    }
}


function reset()
{
    // Reset
    textRecords = new Array();
    textRecords = arrayOfTextRecords[currentSentence];
    
    // Get currentText
    var currentText = "";
    for (i = 0 ; i < textRecords.length ; i++)
    {
        currentText = currentText+textRecords[i].text+" ";
    }
    
    // Create textNode
    textNode=document.getElementById("textNode");
    if(textNode!=null)
        textNode.parentNode.removeChild(textNode);
    textNode = document.createElementNS(svgNS,"text");
    textNode.setAttribute("id","textNode");
    textNode.setAttributeNS(null,"x",10);
    textNode.setAttributeNS(null,"y",30);
    textNode.setAttribute("style",defaultStyleText);
    
    // Fill text node
    var textValue = document.createTextNode(currentText);
    textNode.appendChild(textValue);
    
    svgDocument.appendChild(textNode);
    create_multiline(textNode);
}




function create_multiline(target)
{
    // init var
    tspanArray = new Array();
    var newLine = false;
    var auxHeight = 0;
    var j = 1;
    
    var text_element = target;
    var words = text_element.firstChild.data.split(' ');
    var start_x = text_element.getAttributeNS(null, 'x');
    
    var tspan_element = document.createElementNS(svgNS, "tspan");	// Create first tspan element
    var text_node = document.createTextNode(words[0]);              // Create text in tspan element
    
    tspanArray[0] = tspan_element;
    tspan_element.appendChild(text_node);							// Add tspan element to DOM
    text_element.firstChild.data = '';
    text_element.appendChild(tspan_element);						// Add text to tspan element
    tspan_element.setAttributeNS(null, "x", start_x);
    tspan_element.setAttributeNS(null, "dy", 0);
    
    for(var i = 1; i<words.length; i++)
    {
        var len = tspan_element.firstChild.data.length;				// Find number of letters in string
        
        if(auxHeight + defaultYSpace >= currentHeight)
        {
            tspan_element.setAttribute("visibility","hidden");
        }
        if(words[i]=="{NEWLINE}")
        {
            newLine=true;
        }
        else if(words[i]=="{SCROLL}")
        {
            // On ne devrait plus se retrouver dans ce cas
            //            scrollRecords[z].indexSpan=j-1;
            //            z++;
        }
        else
        {
            tspan_element.firstChild.data += " " + words[i];			// Add next word
        }
        
        if(newLine)
        {
            tspan_element.firstChild.data += " ";
            
            var tspan_element = document.createElementNS(svgNS, "tspan");		// Create new tspan element
            tspanArray[j]=tspan_element;
            j++;
            tspan_element.setAttributeNS(null, "x", start_x);
            tspan_element.setAttributeNS(null, "dy", defaultYSpace);
            auxHeight += defaultYSpace;
            
            text_node = document.createTextNode("");
            newLine = false;
            
            tspan_element.appendChild(text_node);
            text_element.appendChild(tspan_element);
        }
    }
    
}


function createTextSpans(){
    
    tspanArray = tspanArray.slice(0,tspanArray.length);
    
    for(i = 0 ; i < textRecords.length ; i++)
    {
        if(textRecords[i].text.indexOf("{NEWLINE}") != -1)
        {
            // Remove {NEWLINE}
            textRecords[i].text = textRecords[i].text.replace("{NEWLINE}","");
        }
        
        for(j = 0 ; j < tspanArray.length ; j++)
        {
            if(tspanArray[j].textContent.indexOf(textRecords[i].text) != -1)
            {
                var textAux = tspanArray[j].textContent.substr(tspanArray[j].textContent.indexOf(textRecords[i].text), textRecords[i].text.length);
                var tspan_element = document.createElementNS(svgNS, "tspan");
                var text_node = document.createTextNode(textAux);
                tspan_element.appendChild(text_node);
                
                var node = tspanArray[j].firstChild;
                if(node.nodeType==3) // noeud de texte
                {
                    node.textContent=node.textContent.replace(textRecords[i].text,"");
                }
                // Add new
                tspanArray[j].appendChild(tspan_element);
                
                // set tspan attributes
                var gradIndex = i;
                for(var l = 0 ; l < currentSentence ; l++)
                {
                    gradIndex += arrayOfTextRecords[l].length;
                }
                
                tspan_element.setAttribute("fill","url(#gradientDefinition_"+gradIndex+")");
                
                var text_karaoke_color=document.getElementById("text_karaoke_color_"+gradIndex);
                text_karaoke_color.setAttribute("style","stop-color:#" +currentKaraokeColor);
                
                var text_normal_color=document.getElementById("text_karaoke_normal_"+gradIndex);
                text_normal_color.setAttribute("style","stop-color:#" +currentTextColor );
                
                var text_karaoke_animation=document.getElementById("text_animation_"+gradIndex);
                text_karaoke_animation.setAttribute("begin",textRecords[i].start);
                text_karaoke_animation.setAttribute("dur",textRecords[i].end);
                
                break;
            }
            else
            {
                //   alert('NON : tspanArray['+j+'].textContent:'+tspanArray[j].textContent)
            }
        }
    }
}


function timer()
{
    
 /*   setInterval(function()
                {
                if(seconds > timeToScrollArray[currentSentence])
                {
                currentSentence += 1;
                reset();
                createTextSpans();
                }
                seconds += 0.10;
                }, 100);
    */
     var timeToWait = timeToScrollArray[currentSentence];
     if(currentSentence > 0)
     timeToWait = timeToWait - timeToScrollArray[currentSentence - 1];
//     alert(timeToWait);
     timeToWait *= 1000;
     setTimeout(function(){
     launchNextView();
     }, timeToWait);
     
}

function launchNextView(){
    
    currentSentence += 1;
    //    alert(timeToScrollArray.length);
    //  alert(currentSentence);
    if(currentSentence <= timeToScrollArray.length)
    {
        reset();
        createTextSpans();
        
        timer();
    }
}


