//
//  AppDelegate.m
//  Zoolane
//
//  Created by Developer IMX on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashView.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "UIDevice+IdentifierAddition.h"
#import "iRate.h"
#import "HomeController.h"

@implementation AppDelegate
@synthesize  controller;
@synthesize facebook;
@synthesize window = _window;
@synthesize hasJustEnteredForeground;

+ (void)initialize
{
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].previewMode = NO;
    [iRate sharedInstance].usesUntilPrompt = 9999;
    [iRate sharedInstance].eventsUntilPrompt = 3;
    [iRate sharedInstance].daysUntilPrompt = 0.f;
    [iRate sharedInstance].remindPeriod = 0.f;
    [iRate sharedInstance].promptAtLaunch = NO;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    hasJustEnteredForeground = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    controller = [[ApplicationController alloc] init];
    
    facebook = [[Facebook alloc] initWithAppId:@"216339055175764" andDelegate:self];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    NSLog(@"Registering for push notifications...");
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeAlert |
      UIRemoteNotificationTypeBadge |
      UIRemoteNotificationTypeSound)];
    
    return YES;
}


// Notif


- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    //NSString *str = [NSString stringWithFormat:@"Device Token=%@", deviceToken];
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<> "]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSLog(@"Device Token=%@", deviceToken);
   // str = [NSString stringWithFormat:@"Cleared Token=%@", token];
    NSLog(@"Cleared Token=%@", token);
    UIRemoteNotificationType rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    NSLog(@"UIRemoteNotificationType = %d", rntypes);
    
    // Get the users Device Model, Display Name, Unique ID, Token & Version Number
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceUuid = dev.uniqueDeviceIdentifier;
    NSLog(@"deviceUuid : %@", deviceUuid);
    NSString *deviceName = dev.name;
    NSString *deviceModel = dev.model;
    NSString *deviceSystemVersion = dev.systemVersion;
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];

    // langue
    NSString *lang;
    NSString *languageOfTheUser = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if([languageOfTheUser isEqualToString:@"fr"])
    {
        lang = @"FR";
    }
    else if([languageOfTheUser isEqualToString:@"es"])
    {
        lang = @"ES";
    }
    else
    {
        lang = @"EN";
    }



    NSString* request = [NSString stringWithFormat:@"http://millimagespush.com/apns/apns.php?task=register&appname=%@&appversion=%@&deviceuid=%@&devicetoken=%@&devicename=%@&devicemodel=%@&deviceversion=%@&pushbadge=enabled&pushalert=enabled&pushsound=enabled&lang=%@", appName, appVersion, deviceUuid, token, deviceName, deviceModel, deviceSystemVersion, lang];
    
    NSLog(@"request=%@", request);
    NSURL* requestUrl = [NSURL URLWithString:[request stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest* req = [ASIHTTPRequest requestWithURL:requestUrl];
    [req setDelegate:self];
    [req startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    // Use when fetching text data
    NSString *responseString = [request responseString];

    NSLog(@"Request response : %@", responseString);
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"Request error : %@", [error description]);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError -> Error: %@", [err description]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }    
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [controller deleteFileInFileNameToDeleteArray];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [controller deleteFileInFileNameToDeleteArray];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    hasJustEnteredForeground = YES;
    id currentController = [(UINavigationController*)[[self window] rootViewController] visibleViewController];
    if([currentController respondsToSelector:@selector(rate)])
    {
        HomeController* hc = (HomeController*)currentController;
        [hc rate];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    [FBSession.activeSession handleDidBecomeActive];
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSLog(@"openURL");
    return [FBSession.activeSession handleOpenURL:url];
}






/**
 * Called when the user successfully logged in.
 */
- (void)fbDidLogin
{
    NSLog(@"fbDidLogin");
}

/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled
{
    NSLog(@"fbDidNotLogin");
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt
{
    NSLog(@"fbDidExtendToken->%@", accessToken);
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout
{
    NSLog(@"fbDidLogout");
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated
{
    NSLog(@"fbSessionInvalidated");
}


@end
