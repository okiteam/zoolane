//
//  RecordViewController.h
//  Zoolane
//
//  Created by Developer IMX on 9/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "FTAnimationManager.h"
#import "FTAnimation.h"
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CAAnimation.h>
#import "TrackSelectController.h"
#import "RecordTypeView.h"
#import "RecordNameView.h"
#import "RecordTrackView.h"
#import "ChapterSelectView.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"
#import "EGOTextView.h"

@interface RecordViewController : UIViewController<RecordTypeDelegate,RecordTrackDelegate,RecordNameDelegate,ChapterDelegate,UIPopoverControllerDelegate,UIWebViewDelegate, NSXMLParserDelegate>{
    FPPopoverController *popoverControllerIphone;
    UIPopoverController *popoverController;
    ApplicationController *controller;
    SoftwareModel *model;
    NSMutableDictionary *selectedTrack;
    MPMoviePlayerController *moviePlayer;

    CGRect originalPlayFrame;
    CGRect originalRecordFrame;
    BOOL playingChapter;
    BOOL nextDirection;
    BOOL hasRecorded;
    BOOL isRecording;
    BOOL isPlaying;

    BOOL shouldAnimate;
    BOOL auxSwiping;
    
    BOOL auxTrackSelect;
    
    NSTimer *timer;
    float seconds;
    NSString *textString;
    
    //POPUPS
    ChapterSelectView *popupChapterView;
    RecordTypeView *popupRecordType;
    RecordNameView *popupRecordName;
    RecordTrackView *popupRecordTrackView;
}

#pragma mark Properties

@property (weak, nonatomic) IBOutlet UIImageView *imageScreenshot;
@property (weak, nonatomic) IBOutlet UIImageView *imageKaraokeBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imageVideoScreenshot;
@property (weak, nonatomic) IBOutlet UIView *viewScreenshot;
@property (weak, nonatomic) IBOutlet UIView *viewRecord;
@property (weak, nonatomic) IBOutlet UIView *viewMovie;
@property (weak, nonatomic) IBOutlet UIView *viewKaraoke;
//@property (weak, nonatomic) IBOutlet UIView *viewTextKaraoke;
@property (weak, nonatomic) IBOutlet UIWebView *webViewText;

@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecord;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecording;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrev;
@property (weak, nonatomic) IBOutlet UIButton *buttonChapter;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;

#pragma mark Actions
- (IBAction)pressedHome:(id)sender;
- (IBAction)pressedChapter:(id)sender;
- (IBAction)pressedNextPage:(id)sender;
- (IBAction)pressedPrevPage:(id)sender;
- (IBAction)pressedPlay:(id)sender;
- (IBAction)pressedRecord:(id)sender;
- (IBAction)pressedRecording:(id)sender;


@end
