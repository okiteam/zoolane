//
//  LanguageController.m
//  Zoolane
//
//  Created by Developer IMX on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LanguageController.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"

@interface LanguageController ()

@end

@implementation LanguageController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;

    
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}





- (IBAction)frenchPressed:(id)sender {
    
    [LanguageModel setLocale:@"fr"];    
    [delegate clickedLanguage];
   /// NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
   // NSMutableArray* languages=[userDefaults objectForKey:@"AppleLanguageS"];
}

- (IBAction)englishPressed:(id)sender {
    [LanguageModel setLocale:@"en"];    
    [delegate clickedLanguage];

}

- (IBAction)spanishPressed:(id)sender {
}

- (IBAction)brazilPressed:(id)sender {
}
@end
