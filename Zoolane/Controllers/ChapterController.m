//
//  ChapterController.m
//  Zoolane
//
//  Created by Developer IMX on 8/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChapterController.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "iCarousel.h"
#import "BookModel.h"
#import "AppDelegate.h"
#import "Page.h"

@implementation ChapterController
@synthesize carousel;
@synthesize  delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;

    [self loadChapters];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    carousel = nil;
    [self setCarousel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) viewWillDisappear:(BOOL)animated{
    NSLog(@"Desaparece");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

-(void) loadChapters{
    
    carousel.type=iCarouselTypeLinear;
    
    for(int i=0;i<model.book.pages.count;i++){
        [carousel insertItemAtIndex:i animated:TRUE];
    }
    [carousel setCurrentItemIndex:(model.book.pages.count/2)-1];
}


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return model.book.pages.count;
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    
    Page* page=[model.book.pages objectAtIndex:index];
    UILabel *label;
    //create new view if no view is available for recycling
    if (view == nil)
    {
        if(INTERFACE_IS_PHONE){
            view = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 110)] autorelease];
        }
        else{
            view = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 110)] autorelease];
        }
        label = [[[UILabel alloc] initWithFrame:view.bounds] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
        
    }
    else{
        label = (UILabel *)[view viewWithTag:1];
    }
 /*   
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)] autorelease];
        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        label = [[[UILabel alloc] initWithFrame:view.bounds] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [[items objectAtIndex:index] stringValue];*/
    
    NSString *thumbPath=[model.pathBook stringByAppendingPathComponent:page.thumbName];
    UIImage *thumb=[UIImage imageWithContentsOfFile:thumbPath];
    ((UIImageView*)view).image=thumb;
    label.text=[NSString stringWithFormat:@"%d",index+1];
    NSLog(@"%@",thumbPath);

    /*
    NSString *thumbPath=[model.pathSource stringByAppendingPathComponent:[NSString stringWithFormat:@"Templates/%@",[product valueForKey:@"Thumbnail"] ]];
    UIImage *thumb=[UIImage imageWithContentsOfFile:thumbPath];
    
    ProductView *productView=(ProductView*)view;
    productView.frontView.name.text=[product valueForKey:@"Name"];
    productView.frontView.imageProduct.image=thumb;
    
    productView.backView.name.text=[product objectForKey:@"Name"];
    productView.backView.description.text=[product objectForKey:@"Description"];
    int key=[[product valueForKey:@"MinimumPages"] intValue];
    float priceValue=[[[product  objectForKey:@"Prices"] valueForKey:[NSString stringWithFormat:@"%d",key]] floatValue];
    NSString *price=[NSString stringWithFormat:@"From %.2f €",priceValue];
    
    productView.backView.price.text=price;
    productView.backView.pages.text=[NSString stringWithFormat:NSLocalizedString(@"Products_Pages_Info",@"Minimum pages : %d - Maximum pages: %d"), [[product valueForKey:@"MinimumPages"] intValue], [[product valueForKey:@"MaximumPages"] intValue]];
    
    
    productView.backView.imageProduct.image=thumb;
    */
    return view;
}

- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{ 
    
   // if(selectedIndex==index)
        [[self delegate] clickedChapter:index];
    //else {
    //    selectedIndex=index;
   // }

}

-(void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    selectedIndex=-1;
}




@end
