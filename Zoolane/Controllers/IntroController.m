//
//  IntroController.m
//  Zoolane
//
//  Created by Developer IMX on 8/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IntroController.h"
#import "LanguageModel.h"
#import "SplashView.h"
#import "AppDelegate.h"


@interface IntroController ()

@end

@implementation IntroController
@synthesize videoView, skipIntroButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)applicationDidEnterBackground:(NSNotification*)notification 
{
    [moviePlayer pause];
}

-(void)applicationDidBecomeActive:(NSNotification*)notification 
{
    if(!controller.model.showSplash)
        [moviePlayer play];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    [moviePlayer.view removeFromSuperview];
    moviePlayer = nil;
    [self initView];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    
    //[self setVideoView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(controller.model.showSplash)
        controller.model.showSplash=false;
    else
        [self playIntro];

}

-(void)moviePlayBackDidFinish:(NSNotification*)notification 
{  
    moviePlayer = [notification object];  
    [self homePressed:nil];
}

-(void) splashClose{
    
    skipIntroButton.enabled = YES;
    skipIntroButton.hidden = NO;
    [self playIntro];
}

-(void)playIntro{
    
    [moviePlayer prepareToPlay];
    [moviePlayer performSelector:@selector(play) withObject:nil afterDelay:0.f];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void) initView{
    
    self.view.backgroundColor = [UIColor blackColor];
    
    //Splash loader only first time*/
    if(controller.model.showSplash){
        SplashView *splash=[[SplashView alloc] initWithFrame:self.view.frame];
        splash.delegate=self;
        [self.view addSubview:splash];
        skipIntroButton.enabled = NO;
        skipIntroButton.hidden = YES;
    }
    
    NSString *movieFile=[LanguageModel key2String:@"INTRO_VIDEO_FILE_PATH"];
    NSURL *urlOfMovie = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:movieFile ofType:@"mp4"]];
    
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:urlOfMovie];    
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = NO;
    moviePlayer.view.userInteractionEnabled = NO;
    
    [self.videoView addSubview:moviePlayer.view];
    [self.videoView  bringSubviewToFront:moviePlayer.view];
    
    
    if(UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom])
    {
        [moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    }
    else
    {
//        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        [moviePlayer.view setFrame:CGRectMake(0, 0, 480, 320)];
        moviePlayer.view.center = CGPointMake(X_CENTER, Y_CENTER);
    }
    NSLog(@"x, y : %f, %f", moviePlayer.view.center.x, moviePlayer.view.center.y);
    
    moviePlayer.view.backgroundColor = [UIColor clearColor];
    moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
    
    [moviePlayer setFullscreen:YES animated:YES];
    
//    [moviePlayer prepareToPlay];

  //  if(!controller.model.showSplash)
   //    [self splashClose];
}

- (IBAction)homePressed:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil]; 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    if(sender)
        [moviePlayer stop];
    [self performSegueWithIdentifier:@"segueIntroToHomeIdentifier" sender:self];
}

@end
