//
//  RecordTypeController.h
//  Zoolane
//
//  Created by Developer IMX on 9/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioPlayer.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol RecordTypeControllerDelegate

- (void)clickedRecordType:(int) recordType;

@end

@interface RecordTypeController : UIViewController{
    AudioPlayer *audioPlayer;
    SoftwareModel *model;
    ApplicationController *controller;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonStory;
@property (weak, nonatomic) IBOutlet UIButton *buttonConversation;
@property (weak, nonatomic) IBOutlet UIButton *buttonNarrations;
@property (nonatomic,assign) id <RecordTypeControllerDelegate> delegate;
- (IBAction)pressedWholeStory:(id)sender;
- (IBAction)pressedNarrator:(id)sender;
- (IBAction)pressedDialogs:(id)sender;

@end
