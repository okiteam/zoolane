//
//  InfoController.h
//  Zoolane
//
//  Created by Developer IMX on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"


@interface InfoController : UIViewController{
    ApplicationController *controller;
    SoftwareModel *model;
}

- (IBAction)homePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *labelInfo;

@end
