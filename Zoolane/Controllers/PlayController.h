//
//  PlayController.h
//  Zoolane
//
//  Created by Developer IMX on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
//#import "ChapterController.h"
#import "OHAttributedLabel.h"
#import "EGOTextView.h"
#import "FPPopoverController.h"
#import "LrcRecord.h"
//#import "TrackSelectController.h"
#import "PlaySelectTrackView.h"
#import "ChapterSelectView.h"

@interface PlayController : UIViewController<UIPopoverControllerDelegate,ChapterDelegate,TrackSelectControllerDelegate,UIWebViewDelegate>
{
    ApplicationController *controller;
    SoftwareModel *model;
    MPMoviePlayerController *moviePlayer;
    
    NSString *videoPath;
    NSString *videoFile;
    BOOL mute;
    UIPopoverController *popover;
    FPPopoverController *popoverIphone;
    NSTimer *timer;
    NSString *textString;
    double seconds;
    
    BOOL nextDirection;
    BOOL shouldAnimate;
    BOOL auxSwiping;
    BOOL startPlaying;
    
    float auxChars;
    LrcRecord *activeRecord;
    
    NSMutableDictionary *selectedTrack;
    
    //POPUPS
    PlaySelectTrackView *popupPlayView;
    ChapterSelectView *popupChapterView;

    // TEST YANN
//    UIImageView* imViewTest;
    
}
@property (weak, nonatomic) IBOutlet UIWebView *webviewText;
#pragma mark Properties
@property (weak, nonatomic) IBOutlet UIView *karaokeView;
@property (weak, nonatomic) IBOutlet UIView *viewScreenshot;
@property (weak, nonatomic) IBOutlet UIImageView *imageScreenshot;
@property (weak, nonatomic) IBOutlet UIImageView *imageKaraokeScreenshot;
@property (weak, nonatomic) IBOutlet UIView *recordView;

@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIButton *buttonMute;
@property (weak, nonatomic) IBOutlet UIButton *buttonChapter;
@property (weak, nonatomic) IBOutlet UIButton *buttonNextPage;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrevPage;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageBackground;

#pragma mark Actions
- (IBAction)chapterPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)nextChapter:(id)sender;
- (IBAction)prevChapter:(id)sender;
- (IBAction)mutePressed:(id)sender;

-(void)update:(NSTimer*)timer;

@end
