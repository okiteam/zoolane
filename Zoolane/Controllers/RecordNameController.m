//
//  RecordNameController.m
//  Zoolane
//
//  Created by Developer IMX on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecordNameController.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"

@interface RecordNameController ()

@end

@implementation RecordNameController
@synthesize labelTitle;
@synthesize textName;
@synthesize buttonOK;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    if(INTERFACE_IS_PHONE)
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE_IPHONE]];
    else {
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE]];
    }
    
    [buttonOK setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    labelTitle.text=[LanguageModel key2String:@"RECORD_NAME_CAPTION"];
    
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_NAME_STORY"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    if(INTERFACE_IS_PHONE)
        [textName becomeFirstResponder];
    
}

- (void)viewDidUnload
{
    [self setLabelTitle:nil];
    [self setTextName:nil];
    [self setButtonOK:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) viewDidAppear:(BOOL)animated{
    [audioPlayer play];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

-(void) viewWillAppear:(BOOL)animated{
    [buttonOK setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    labelTitle.text=[LanguageModel key2String:@"RECORD_NAME_CAPTION"];

}
- (IBAction)clickedOK:(id)sender {
    if(![textName.text isEqualToString:@""]){
        [textName resignFirstResponder];
        [delegate clickedNameOK:textName.text];
    }
}

- (IBAction)textExit:(id)sender{
    [sender resignFirstResponder];
}
-(BOOL) canBecomeFirstResponder{
    return TRUE;
}
@end
