//
//  NewsletterController.h
//  Zoolane
//
//  Created by Developer IMX on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol NewsletterDelegate

- (void)clickedOK;

@end

@interface NewsletterController : UIViewController<UITextFieldDelegate>{
    ApplicationController *controller;
    SoftwareModel *model;
    NSMutableData *responseData;

    
}
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *labelTitle;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonRegister;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *textEmail;

@property (nonatomic,assign) id <NewsletterDelegate> delegate;

- (IBAction)textDidEndOnExit:(id)sender;

- (IBAction)facebookPressed:(id)sender;
- (IBAction)twitterPressed:(id)sender;
- (IBAction)registerPressed:(id)sender;

@end
