//
//  IntroController.h
//  Zoolane
//
//  Created by Developer IMX on 8/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SplashView.h"

@interface IntroController : UIViewController<SplashDelegate>
{
    MPMoviePlayerController *moviePlayer;
    ApplicationController *controller;
    SoftwareModel *model;
}
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIButton* skipIntroButton;

-(void)playIntro;
- (IBAction)homePressed:(id)sender;

@end
