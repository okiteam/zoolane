//
//  ChapterController.h
//  Zoolane
//
//  Created by Developer IMX on 8/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoftwareModel.h"
#import "ApplicationController.h"
#import "iCarousel.h"

@protocol ChapterDelegate

- (void)clickedChapter:(int) chapterIndex;

@end

@interface ChapterController : UIViewController<iCarouselDataSource,iCarouselDelegate>
{
    SoftwareModel *model;
    ApplicationController *controller;
    int selectedIndex;
    
    
}
@property (nonatomic,assign) id <ChapterDelegate> delegate;

@property (unsafe_unretained, nonatomic) IBOutlet iCarousel *carousel;


@end
