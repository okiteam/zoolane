//
//  RecordNameController.h
//  Zoolane
//
//  Created by Developer IMX on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol RecordNameControllerDelegate

- (void)clickedNameOK:(NSString*)name;

@end

@interface RecordNameController : UIViewController<UITextFieldDelegate>{
    ApplicationController *controller;
    SoftwareModel *model;
    AudioPlayer *audioPlayer;

}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
- (IBAction)textExit:(id)sender;

@property (nonatomic,assign) id <RecordNameControllerDelegate> delegate;

- (IBAction)clickedOK:(id)sender;

@end
