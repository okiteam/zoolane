//
//  HomeController.h
//  Zoolane
//
//  Created by Developer IMX on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "FPPopoverController.h"
#import "LanguageView.h"
#import "FacebookChoiceView.h"
#import "NewsletterView.h"
#import "FaceBookLikeButton.h"

@interface HomeController : UIViewController<NewsletterDelegate,LanguageDelegate, FacebookChoiceDelegate, FBDialogDelegate/*, FacebookLikeViewDelegate*/>{
    ApplicationController *controller;
    SoftwareModel *model;
    UIPopoverController *popoverController;
    FPPopoverController *popoverControllerIphone;
    LanguageView *langView;
    NewsletterView *newsView;
    FacebookChoiceView *facebookView;
    FBLikeButton* buttonLikeView;
}
@property (weak, nonatomic) IBOutlet UIButton *buttonNewsletter;
@property (weak, nonatomic) IBOutlet UIButton *buttonFacebook;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwitter;
@property (weak, nonatomic) IBOutlet UIButton *buttonLanguages;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayMode;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecordMode;
@property (weak, nonatomic) IBOutlet UIImageView *imageTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelOtherApplications;

- (IBAction)logoPressed:(id)sender;
- (IBAction)languagePressed:(id)sender;
- (IBAction)applicationsPressed:(id)sender;
- (IBAction)youtubePressed:(id)sender;
- (IBAction)newsletterPressed:(id)sender;
- (IBAction)facebookPressed:(id)sender;
- (IBAction)twitterPressed:(id)sender;
-(void)rate;

@end
