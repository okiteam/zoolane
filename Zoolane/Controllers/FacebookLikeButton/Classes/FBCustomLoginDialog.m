//
//  Facebook+ForceDialog.m
//  Yardsellr
//
//  Created by Thomas Brow on 12/10/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//


#import "FBCustomLoginDialog.h"

@implementation FBCustomLoginDialog
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	NSURL* url = request.URL;
	if ([[url absoluteString] rangeOfString:@"login"].location == NSNotFound) {
		[self dialogDidSucceed:url];
		return NO;
	}
	else if (url != nil) {
		[_spinner startAnimating];
		[_spinner setHidden:NO];
		return YES;
	}
	return NO;
}
@end
