//
//  FacebookLikeView.h
//  Yardsellr
//
//  Created by Tom Brow on 5/9/11.
//  Copyright 2011 Yardsellr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBCustomLoginDialog.h"

#define FB_LIKE_BUTTON_LOGIN_NOTIFICATION @"FBLikeLoginNotification"

typedef enum {
	FBLikeButtonStyleStandard,
	FBLikeButtonStyleButtonCount,
	FBLikeButtonStyleBoxCount
} FBLikeButtonStyle;

typedef enum {
	FBLikeButtonColorLight,
	FBLikeButtonColorDark
} FBLikeButtonColor;

@interface FBLikeButton : UIView<UIWebViewDelegate, FBDialogDelegate> {
	UIWebView *webView_;
	UIColor *textColor_;
	UIColor *linkColor_;
	UIColor *buttonColor_;
}
@property(nonatomic) UIColor *textColor;
@property(nonatomic) UIColor *linkColor;
@property(nonatomic) UIColor *buttonColor;

- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage andStyle:(FBLikeButtonStyle)style andColor:(FBLikeButtonColor)color andLang:(NSString*)lang;
- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage andLang:(NSString*)lang;

@end