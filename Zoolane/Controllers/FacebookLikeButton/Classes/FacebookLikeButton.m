//
//  FacebookLikeView.m
//  Yardsellr
//
//  Created by Tom Brow on 5/9/11.
//  Copyright 2011 Yardsellr. All rights reserved.
//

#import "FacebookLikeButton.h"

//FBLikeButton.m
//LoginDialog es estatica para abrir unicamente un login en toda la app
static FBDialog *loginDialog_;

@implementation FBLikeButton

@synthesize textColor = textColor_, buttonColor = buttonColor_, linkColor = linkColor_;

- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage andStyle:(FBLikeButtonStyle)style andColor:(FBLikeButtonColor)color andLang:(NSString*)lang{
	if ((self = [super initWithFrame:frame])) {
		NSString *styleQuery=(style==FBLikeButtonStyleButtonCount? @"button_count" : (style==FBLikeButtonStyleBoxCount? @"box_count" : @"standard"));
		NSString *colorQuery=(color==FBLikeButtonColorDark? @"dark" : @"light");
		NSString *url =[NSString stringWithFormat:@"http://www.facebook.com/plugins/like.php?locale=%@&layout=%@&show_faces=false&width=%d&height=%d&action=like&colorscheme=%@&href=%@", lang, styleQuery, (int)frame.size.width, (int)frame.size.height, colorQuery, likePage];
        
		//Creamos una webview muy alta para evitar el scroll interno por la foto del usuario y otras cosas
		webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 300)];
		[self addSubview:webView_];
		[webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
		webView_.opaque = NO;
		webView_.backgroundColor = [UIColor clearColor];
		webView_.delegate = self;
		webView_.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        for (UIView *v in webView_.subviews) {
            if([v isKindOfClass:[UIScrollView class]])
            {
                UIScrollView* scroll = (UIScrollView*)v;
                [scroll setBounces:NO];
            }
        }
		self.backgroundColor=[UIColor clearColor];
		self.clipsToBounds=YES;
        
		[[NSNotificationCenter defaultCenter] addObserver:webView_ selector:@selector(reload) name:FB_LIKE_BUTTON_LOGIN_NOTIFICATION object:nil];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage andLang:(NSString*)lang{
	return [self initWithFrame:frame andUrl:likePage andStyle:FBLikeButtonStyleStandard andColor:FBLikeButtonColorLight andLang:lang];
}

- (void)dealloc {
    
	[[NSNotificationCenter defaultCenter] removeObserver:webView_ name:FB_LIKE_BUTTON_LOGIN_NOTIFICATION object:nil];
    
	[webView_ stopLoading];
	webView_.delegate=nil;
	[webView_ removeFromSuperview];
    
	self.linkColor=nil;
	self.textColor=nil;
	self.buttonColor=nil;
}


- (void) configureTextColors{
	NSString *textColor = @"FFFFFF";//[textColor_ hexStringFromColor];
	NSString *buttonColor = @"00FF00";//[buttonColor_ hexStringFromColor];
	NSString *linkColor = @"0000FF";//[linkColor_ hexStringFromColor];
    
	NSString *javascriptLinks = [NSString stringWithFormat:@"{"
                                 "var textlinks=document.getElementsByTagName('a');"
                                 "for(l in textlinks) { textlinks[l].style.color='#%@';}"
                                 "}", linkColor];
    
	NSString *javascriptSpans = [NSString stringWithFormat:@"{"
                                 "var spans=document.getElementsByTagName('span');"
                                 "for(s in spans) { if (spans[s].className!='liketext') { spans[s].style.color='#%@'; } else {spans[s].style.color='#%@';}}"
                                 "}", textColor, (buttonColor==nil? textColor : buttonColor)];
    
	//Lanzamos el javascript inmediatamente
	if (linkColor)
		[webView_ stringByEvaluatingJavaScriptFromString:javascriptLinks];
	if (textColor)
		[webView_ stringByEvaluatingJavaScriptFromString:javascriptSpans];
    
	//Programamos la ejecucion para cuando termine
	if (linkColor)
		[webView_ stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setTimeout(function () %@, 3000)", javascriptLinks]];
	if (textColor)
		[webView_ stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setTimeout(function () %@, 3000)", javascriptSpans]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
	if (loginDialog_!=nil)
		return NO;
    
	// if user has to log in, open a new (modal) window
	if ([[[request URL] absoluteString] rangeOfString:@"login.php"].location!=NSNotFound){
		loginDialog_= [[FBCustomLoginDialog alloc] init];
		[loginDialog_ loadURL:[[request URL] absoluteString] get:nil];
		loginDialog_.delegate = self;
		[loginDialog_ show];
		return NO;
	}
	if (([[[request URL] absoluteString] rangeOfString:@"/connect/"].location!=NSNotFound) || ([[[request URL] absoluteString] rangeOfString:@"like.php"].location!=NSNotFound)){
		return YES;
	}
    
	NSLog(@"URL de Facebook no contemplada: %@", [[request URL] absoluteString]);
    
	return NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	[self configureTextColors];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Facebook Connect

- (void)dialogDidSucceed:(FBDialog*)dialog
{
	loginDialog_.delegate=nil;
	loginDialog_=nil;
    
	//Lanzamos la notificacion para que se actualicen los botones
	[[NSNotificationCenter defaultCenter] postNotificationName:FB_LIKE_BUTTON_LOGIN_NOTIFICATION object:nil];
}

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidComplete:(FBDialog *)dialog{
	[self dialogDidSucceed:dialog];
}

/**
 * Called when the dialog succeeds with a returning url.
 */
- (void)dialogCompleteWithUrl:(NSURL *)url{
	[self dialogDidSucceed:loginDialog_];
}

/**
 * Called when the dialog get canceled by the user.
 */
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url{
	[self dialogDidSucceed:loginDialog_];
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidNotComplete:(FBDialog *)dialog{
	[self dialogDidSucceed:loginDialog_];
}

/**
 * Called when dialog failed to load due to an error.
 */
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error{
	[self dialogDidSucceed:loginDialog_];
}

@end