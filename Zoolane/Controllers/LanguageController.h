//
//  LanguageController.h
//  Zoolane
//
//  Created by Developer IMX on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol LanguageDelegate

- (void)clickedLanguage;

@end


@interface LanguageController : UIViewController{
    SoftwareModel *model;
    ApplicationController *controller;
    
}

@property (nonatomic,assign) id <LanguageDelegate> delegate;


- (IBAction)frenchPressed:(id)sender;
- (IBAction)englishPressed:(id)sender;
- (IBAction)spanishPressed:(id)sender;
- (IBAction)brazilPressed:(id)sender;

@end
