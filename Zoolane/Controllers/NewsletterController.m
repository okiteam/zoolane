//
//  NewsletterController.m
//  Zoolane
//
//  Created by Developer IMX on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewsletterController.h"
#import "SoftwareModel.h"
#import "ApplicationController.h"
#import "AppDelegate.h"

@interface NewsletterController ()

@end

@implementation NewsletterController
@synthesize labelTitle;
@synthesize buttonRegister;
@synthesize textEmail;
@synthesize delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    if(INTERFACE_IS_PAD)
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE]];
    else {
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE_IPHONE]];

    }   
    [labelTitle setText:[LanguageModel key2String:@"MENU_EMEIL_CAPTION"]];
    [buttonRegister setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    
    textEmail.delegate=self;
    if(INTERFACE_IS_PHONE)
        [textEmail becomeFirstResponder];

	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [labelTitle setText:[LanguageModel key2String:@"MENU_EMEIL_CAPTION"]];
    [buttonRegister setTitle:[LanguageModel key2String:@"OK"] forState:UIControlStateNormal];
    [textEmail becomeFirstResponder];
    
}

- (void)viewDidUnload
{
    [self setLabelTitle:nil];
    [self setButtonRegister:nil];
    [self setTextEmail:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)textDidEndOnExit:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)facebookPressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[LanguageModel key2String:@"FACEBOOK_LINK"]]];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

- (IBAction)twitterPressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[LanguageModel key2String:@"TWITTER_LINK"]]];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

- (IBAction)registerPressed:(id)sender {
    
    //Validamos email
    if([ApplicationController validateEmailWithString:textEmail.text]){
        [self registerEmail];
        [delegate clickedOK]; 
    }
    else{
        textEmail.placeholder=@"Email not correct";
    }
    
    /*
    NSURL *url = [NSURL URLWithString:@"http://www.millimages.com/mobiles-app/zoolane/send_email.php"];
    
    NSString *post = [NSString stringWithFormat:@"email=%@", email];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSURLConnection* theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
*/
}

-(void) registerEmail{
    NSURL *url = [NSURL URLWithString:@"http://www.millimages.com/mobiles-app/zoolane/send_email.php"];
    
    NSString *post = [NSString stringWithFormat:@"email=%@", textEmail.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSURLConnection* theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    //we got the delegation response
    if(theConnection)
    {       
        responseData = [NSMutableData data];
        NSLog(@"%@",responseData);
        NSLog(@"%@",[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    }
    else
    {
       // DLog(@"NewsLetterSubView.m sendEmail : Connection Failed");
    }

}


- (BOOL) canBecomeFirstResponder{
    return YES;
}

@end
