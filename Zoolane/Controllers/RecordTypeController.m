//
//  RecordTypeController.m
//  Zoolane
//
//  Created by Developer IMX on 9/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecordTypeController.h"
#import "LanguageModel.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"


@interface RecordTypeController ()

@end

@implementation RecordTypeController
@synthesize labelTitle;
@synthesize buttonStory;
@synthesize buttonConversation;
@synthesize buttonNarrations;
@synthesize  delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_WHAT_RECORD"]] stringByAppendingString:@".aif"];

    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];

    if(INTERFACE_IS_PHONE)
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE_IPHONE]];
    else {
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE]];
    }
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setLabelTitle:nil];
    [self setButtonStory:nil];
    [self setButtonNarrations:nil];
    [self setButtonConversation:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

-(void) viewWillAppear:(BOOL)animated{
    [buttonStory setTitle:[LanguageModel key2String:@"RECORD_WHOLE_TRACK"] forState:UIControlStateNormal];
    [buttonNarrations setTitle:[LanguageModel key2String:@"RECORD_NARRATION_TRACK"] forState:UIControlStateNormal];
    [buttonConversation setTitle:[LanguageModel key2String:@"RECORD_CONVERSATION_TRACK"] forState:UIControlStateNormal];
    labelTitle.text=[LanguageModel key2String:@"RECORD_TYPE_CAPTION"];
}

-(void) viewDidAppear:(BOOL)animated{
    [audioPlayer play];
}

- (IBAction)pressedWholeStory:(id)sender {
    [delegate clickedRecordType:0];
    [self dismissModalViewControllerAnimated:TRUE];
}

- (IBAction)pressedNarrator:(id)sender {
    [delegate clickedRecordType:1];
    [self dismissModalViewControllerAnimated:TRUE];


}

- (IBAction)pressedDialogs:(id)sender {
    [delegate clickedRecordType:2];
    [self dismissModalViewControllerAnimated:TRUE];


}
@end
