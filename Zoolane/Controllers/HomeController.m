//
//  HomeController.m
//  Zoolane
//
//  Created by Developer IMX on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeController.h"

#import "FPPopoverController.h"
#import "AppDelegate.h"
#import "SoftwareModel.h"
#import "ApplicationController.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "iRate.h"

@interface HomeController ()

@end

@implementation HomeController
@synthesize buttonNewsletter;
@synthesize buttonFacebook;
@synthesize buttonTwitter;
@synthesize buttonLanguages;
@synthesize imageTitle;
@synthesize imageLogo;
@synthesize labelOtherApplications;
@synthesize buttonPlayMode;
@synthesize buttonRecordMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    [controller loadSelectedChapter:nil ofType:0];

    for(UIView* v in self.view.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton* b = (UIButton*)v;
            b.exclusiveTouch = YES;
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(canTweetStatus) name:ACAccountStoreDidChangeNotification object:nil];
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ACAccountStoreDidChangeNotification object:nil];
    [self setLabelOtherApplications:nil];
    [self setImageLogo:nil];
    [self setImageTitle:nil];
    [self setButtonLanguages:nil];
    [self setButtonNewsletter:nil];
    [self setButtonFacebook:nil];
    [self setButtonTwitter:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) viewWillAppear:(BOOL)animated{
    [self reloadLanguage];
    
    [self rate];
}

-(void)rate
{
    AppDelegate* app = ((AppDelegate * )[[UIApplication sharedApplication] delegate]);
    if(app.hasJustEnteredForeground)
    {
        [app setHasJustEnteredForeground:FALSE];
        [[iRate sharedInstance] logEvent:FALSE];
    }
}

-(void) reloadLanguage{
    [imageLogo setImage:[UIImage imageNamed:[LanguageModel key2String:@"MENU_TITLE_IMAGE"]]];
    
    [imageTitle setImage:[UIImage imageNamed:[LanguageModel key2String:@"MENU_LOGO_IMAGE"]]];
    
    [labelOtherApplications setText:[LanguageModel key2String:@"MENU_APP_TEXT"]];
    if(INTERFACE_IS_PAD)
        [self.labelOtherApplications setFont:[UIFont fontWithName:FONT_MAIANDRA size:FONT_SIZE_SMALL]];
    else{
        [self.labelOtherApplications setFont:[UIFont fontWithName:FONT_MAIANDRA size:FONT_SIZE_SMALL_IPHONE]];
    }
    [self.labelOtherApplications setTextColor:[LanguageModel UIColorFromRGB:COLOR_LABEL]];
    [model setTimingDictionary];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
}


#pragma mark Actions

-(void) clickedOK{
    [newsView removeFromSuperview];
}

-(void) clickedLanguage{
    [langView removeFromSuperview];
    [self reloadLanguage];
}

- (IBAction)languagePressed:(id)sender {
    langView=[[LanguageView alloc] initWithFrame:self.view.frame];
    langView.delegate=self;
    [self.view addSubview:langView];    
}

- (IBAction)applicationsPressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
        NSString* stringUrl = nil;
        NSInteger buttonId = [(UIButton*)sender tag];
        switch (buttonId) {
            case 0:
                stringUrl = [LanguageModel key2String:@"APP_ONE_LINK"];
                break;
            case 1:
                stringUrl = [LanguageModel key2String:@"APP_TWO_LINK"];
                break;
            case 2:
                stringUrl = [LanguageModel key2String:@"APP_THREE_LINK"];
                break;
            default:
                NSLog(@"PAS D'URL VALIDE");
                break;
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
    }else{
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];        
    }
}


- (IBAction)youtubePressed:(id)sender {
    if([ApplicationController isNetworkAvailable:true:true]){
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[LanguageModel key2String:@"YOUTUBE_LINK"]]];
    }
    else{
       [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

- (IBAction)newsletterPressed:(id)sender {
    newsView=[[NewsletterView alloc] initWithFrame:self.view.frame];
    newsView.delegate=self;
    [self.view addSubview:newsView];
    [newsView initializeView];
}

- (IBAction)logoPressed:(id)sender {

    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark Facebook/Twitter

- (IBAction)facebookPressed:(id)sender{
    
    NSLog(@"facebookPressed");
    
    if([ApplicationController isNetworkAvailable:true:true])
    {
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session,
           FBSessionState state, NSError *error) {
             [self sessionStateChanged:session state:state error:error];
         }];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


- (IBAction)twitterPressed:(id)sender
{
    NSLog(@"twitterPressed");
    if([ApplicationController isNetworkAvailable:true:true])
    {
        // Set up the built-in twitter composition view controller.
        TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
        
        // Set the initial tweet text. See the framework for additional properties that can be set.
        [tweetViewController setInitialText:[NSString stringWithFormat:@"%@ %@", [LanguageModel key2String:@"SOCIAL_DEFAULT_TEXT"], [LanguageModel key2String:@"APPBITLY"]]];
        
        // Create the completion handler block.
        [tweetViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
            NSString *output;
            
            switch (result) {
                case TWTweetComposeViewControllerResultCancelled:
                    // The cancel button was tapped.
                    output = @"Tweet cancelled.";
                    break;
                case TWTweetComposeViewControllerResultDone:
                    // The tweet was sent.
                    output = @"Tweet done.";
                    break;
                default:
                    break;
            }
            
            //        [self performSelectorOnMainThread:@selector(displayText:) withObject:output waitUntilDone:NO];
            NSLog(@"output : %@", output);
            
            // Dismiss the tweet composition view controller.
            [self dismissModalViewControllerAnimated:YES];
        }];
        
        // Present the tweet composition view controller modally.
        [self presentModalViewController:tweetViewController animated:YES];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:[LanguageModel key2String:@"WARNING_TITLE"] message:[LanguageModel key2String:@"WARNING_NOINTERNET"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

#pragma -
#pragma Facebook

-(void)showLoginView
{
    
    NSLog(@"showLoginView");
    facebookView = [[FacebookChoiceView alloc] initWithFrame:self.view.frame];
    facebookView.facebookChoiceDelegate = self;
    [self.view addSubview:facebookView];
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            NSLog(@"FBSessionStateOpen");
            [self showLoginView];
            break;
        }
        case FBSessionStateClosed:
        {
            NSLog(@"FBSessionStateClosed");
            //[FBSession.activeSession closeAndClearTokenInformation];
            break;
        }
        case FBSessionStateClosedLoginFailed:
        {
            NSLog(@"FBSessionStateClosedLoginFailed");
            [FBSession.activeSession closeAndClearTokenInformation];
            //[self showLoginView];
            break;
        }
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark -
#pragma mark Facebook/Twitter delegate

- (void)facebookButtonPressed:(NSInteger)buttonId
{
    NSLog(@"facebookButtonPressed : %d", buttonId);
    switch (buttonId) {
        case 1000:
            [facebookView removeFromSuperview];
            break;
            
        case 1001:
        {
            NSLog(@"share");
            [self share];
            break;
        }
        default:
            break;
    }
}

-(void)share
{
    Facebook* face = ((AppDelegate * )[[UIApplication sharedApplication] delegate]).facebook;
 
    NSString* propString = [NSString stringWithFormat:@"{\"%@\":{\"href\":\"%@\",\"text\":\"iTunes AppStore\"}}", [LanguageModel key2String:@"LINK"], [LanguageModel key2String:@"APPBITLY"]];
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"216339055175764", @"app_id",
                                   [LanguageModel key2String:@"FACEBOOK_LINK"], @"link",
                                   @"http://www.millimages.com/images/64zlicon.png", @"picture",
                                   [LanguageModel key2String:@"TITRE"], @"name",
                                   [LanguageModel key2String:@"SOCIAL_DEFAULT_TEXT"], @"caption",
                                   propString, @"properties",
                                   nil];
    
    [face dialog:@"feed" andParams:params andDelegate:self];
}

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidComplete:(FBDialog *)dialog
{
    NSLog(@"dialogDidComplete");
}

/**
 * Called when the dialog succeeds with a returning url.
 */
- (void)dialogCompleteWithUrl:(NSURL *)url
{
    NSLog(@"dialogCompleteWithUrl");
}


/**
 * Called when the dialog get canceled by the user.
 */
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url{
    NSLog(@"dialogDidNotCompleteWithUrl");
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidNotComplete:(FBDialog *)dialog{
    NSLog(@"dialogDidNotComplete");
}


/**
 * Called when dialog failed to load due to an error.
 */
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:error.localizedDescription
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];
}

/**
 * Asks if a link touched by a user should be opened in an external browser.
 *
 * If a user touches a link, the default behavior is to open the link in the Safari browser,
 * which will cause your app to quit.  You may want to prevent this from happening, open the link
 * in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you
 * should hold onto the URL and once you have received their acknowledgement open the URL yourself
 * using [[UIApplication sharedApplication] openURL:].
 */
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL *)url{
    NSLog(@"shouldOpenURLInExternalBrowser");
    return NO;
}

// TWEET //
- (void)canTweetStatus
{
    if ([TWTweetComposeViewController canSendTweet])
    {
        NSLog(@"can tweet");
    }
    else
    {
        NSLog(@"cannot tweet");
    }
}


@end
