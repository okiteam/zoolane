//
//  TrackSelectController.h
//  Zoolane
//
//  Created by Developer IMX on 9/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"

@protocol TrackSelectControllerDelegate
- (void)clickedTrackSelect:(int) trackNumber:(BOOL)removeExisting;
@end

@interface TrackSelectController : UIViewController{
    ApplicationController *controller;
    SoftwareModel *model;
    BOOL deleteTrack;
    AudioPlayer *audioPlayer;

    
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack2;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrack3;
@property (weak, nonatomic) IBOutlet UIButton *buttonNewTrack;
@property bool playMode;

@property (nonatomic,assign) id <TrackSelectControllerDelegate> delegate;

- (IBAction)pressedTrack1:(id)sender;
- (IBAction)pressedTrack2:(id)sender;
- (IBAction)pressedTrack3:(id)sender;
- (IBAction)pressedNewTrack:(id)sender;

@end
