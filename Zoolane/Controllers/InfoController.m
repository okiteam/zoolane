//
//  InfoController.m
//  Zoolane
//
//  Created by Developer IMX on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InfoController.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"

@interface InfoController ()

@end

@implementation InfoController
@synthesize labelInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setLabelInfo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillAppear:(BOOL)animated{
    labelInfo.text=[LanguageModel key2String:@"END_TEXT"];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)homePressed:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
