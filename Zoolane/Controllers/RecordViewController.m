//
//  RecordViewController.m
//  Zoolane
//
//  Created by Developer IMX on 9/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecordViewController.h"
#import "RecordTypeView.h"
#import "ApplicationController.h"
#import "AppDelegate.h"
#import "SoftwareModel.h"
#import "FTAnimation.h"
#import "FTAnimationManager.h"
#import "TrackSelectController.h"
#import <AudioToolbox/AudioServices.h>
#import "ChapterSelectView.h"

#import "Page.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"
#import "LrcRecord.h"
#import "EGOTextView.h"

@interface RecordViewController ()

@end

@implementation RecordViewController
@synthesize imageKaraokeBackground;
@synthesize viewScreenshot;
@synthesize imageVideoScreenshot;
@synthesize viewRecord;
@synthesize imageScreenshot;
@synthesize viewMovie;
@synthesize viewKaraoke;
@synthesize webViewText;

@synthesize buttonPlay;
@synthesize buttonRecord;
@synthesize buttonRecording;
@synthesize buttonNext;
@synthesize buttonPrev;
@synthesize buttonChapter;
@synthesize buttonHome;

#pragma mark View Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller = ((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model = controller.model;

    // Notif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Gesture recognizers
    UISwipeGestureRecognizer *swipeGestureRight=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(pressedPrevPage:)];
    UISwipeGestureRecognizer *swipeGestureLeft=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(pressedNextPage:)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    swipeGestureLeft.cancelsTouchesInView=TRUE;
    swipeGestureRight.cancelsTouchesInView=TRUE;
    [self.view addGestureRecognizer:swipeGestureRight];
    [self.view addGestureRecognizer:swipeGestureLeft];
        
    // Audio
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    //  UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    //    AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute,sizeof(audioRouteOverride),&audioRouteOverride);
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof (doChangeDefaultRoute), &doChangeDefaultRoute);
    [session setActive:YES error:nil];
    
    // Video
    moviePlayer = model.moviePlayer;
    [self.viewMovie addSubview:moviePlayer.view];
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = NO;  
    moviePlayer.view.userInteractionEnabled = NO;
    [moviePlayer.view setFrame:viewMovie.frame];
    moviePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    moviePlayer.view.backgroundColor = [UIColor blackColor];
    moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
    [viewMovie bringSubviewToFront:buttonRecording];
    [viewMovie bringSubviewToFront:buttonRecord];
    [viewMovie bringSubviewToFront:buttonPlay];
    
    // Webview
    webViewText.backgroundColor=[UIColor clearColor];
    [webViewText setOpaque:FALSE];
    webViewText.delegate=self;
    [(UIScrollView *) [webViewText.subviews objectAtIndex:0] setAlwaysBounceVertical:NO];
    [(UIScrollView *) [webViewText.subviews objectAtIndex:0] setAlwaysBounceHorizontal:NO];
    webViewText.scrollView.scrollEnabled = NO;
    webViewText.userInteractionEnabled = FALSE;
    for (id subview in webViewText.subviews){
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    }
    
    // Other
    originalPlayFrame = buttonRecording.frame;
    originalPlayFrame.origin.x = (int)(X_CENTER - originalPlayFrame.size.width * 1.2f);
    originalRecordFrame = buttonRecording.frame;
    originalRecordFrame.origin.x = (int)(X_CENTER + originalRecordFrame.size.width * 0.2f);

    // Load first chapter
   [model.book firstPage];
   [self loadChapter];
   [self takeScreenShot];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    hasRecorded = NO;
}

-(void) viewDidAppear:(BOOL)animated{
    [self constructor];
    [self hideControls];
}

-(void) constructor
{
    auxSwiping=TRUE;

    if(![controller anyTrackRecorded]){
        selectedTrack=[controller.settings.records objectForKey:@"Track01"];
        buttonRecord.hidden=TRUE;
        popupRecordType=[[RecordTypeView alloc] initWithFrame:self.view.frame];
        [popupRecordType initializeView];
        popupRecordType.delegate=self;
        [self.view addSubview:popupRecordType];
        [popupRecordType playSound];
    }
    else{
       buttonRecord.hidden=TRUE;
       popupRecordTrackView=[[RecordTrackView alloc] initWithFrame:self.view.frame];
       popupRecordTrackView.delegate=self;
       [popupRecordTrackView initializeView];
       [self.view addSubview:popupRecordTrackView];
       [popupRecordTrackView playSound];                   
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
}

- (void)viewDidUnload
{
    [self setButtonPlay:nil];
    [self setButtonRecord:nil];
    [self setButtonRecording:nil];
    [self setViewKaraoke:nil];
    [self setViewMovie:nil];
    [self setButtonPrev:nil];
    [self setButtonChapter:nil];
    [self setButtonNext:nil];
    [self setButtonHome:nil];
    [self setImageScreenshot:nil];
    [self setViewRecord:nil];
    [self setImageVideoScreenshot:nil];
    [self setViewScreenshot:nil];
    [self setImageKaraokeBackground:nil];
    [self setWebViewText:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}


-(BOOL) canBecomeFirstResponder{
    return NO;
}

#pragma mark Animations


-(void) startAnimation:(CAAnimation *)anim{
    
    NSLog(@"startAnim change page");
}

-(void) stopAnimation:(CAAnimation *)anim finished:(BOOL)flag
{
    [self takeScreenShot];
    [self loadWebView];  
    [self initChapter];
}


-(void)showArrows:(float) duration:(int)repeatCount{
    
    buttonPrev.alpha=0;
    buttonNext.alpha=0;
    

    NSLog(@"Showing arrows");
    BOOL blinking = NO;
    
    if([model.book nextPageExist])
        buttonNext.hidden=FALSE;
    else {
        buttonNext.hidden=TRUE;
    }
    if([model.book prevPageExist])
        buttonPrev.hidden=FALSE;
    else {
        buttonPrev.hidden=TRUE;
    }
    
    NSLog(@"%d %d",buttonPrev.hidden,buttonNext.hidden);
    buttonNext.alpha=0;
    buttonPrev.alpha=0;
    
    [UIView beginAnimations:@"showingArrows" context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationRepeatCount:repeatCount];
    [UIView setAnimationRepeatAutoreverses:YES];
    
    if(!blinking) 
    {       
        if([model.book nextPageExist])
            self.buttonNext.alpha = 1;
        if([model.book prevPageExist])
            self.buttonPrev.alpha = 1;        
    } 
    else 
    {        
        if([model.book nextPageExist])
            self.buttonNext.alpha = 0;
        if([model.book prevPageExist])
            self.buttonPrev.alpha = 0;
    }
    
    blinking = !blinking;
    
    [UIView commitAnimations];

}


-(void) hideControls
{
    buttonRecord.hidden=TRUE;
    buttonPlay.hidden=TRUE;
    buttonHome.hidden=TRUE;
    buttonChapter.hidden=TRUE;
    buttonNext.hidden=TRUE;
    buttonPrev.hidden=TRUE;
}

-(void) hideControlsWhileRecording
{
    [self hideControls];
    [self showRecordingButton];
}

-(void) hideControlsWhilePlaying
{
    [self hideControls];
    buttonPlay.frame = buttonRecording.frame;
    buttonPlay.hidden = FALSE;
    [buttonPlay setSelected:TRUE];
}

-(void)showRecordButton:(BOOL)recordOnly
{
    if(recordOnly)
        buttonRecord.frame = buttonRecording.frame;
    else
        buttonRecord.frame = originalRecordFrame;
    buttonRecord.hidden = FALSE;

    buttonRecord.alpha = 0;
    BOOL blinking=NO;

    [UIView beginAnimations:@"showRecordButton" context:NULL];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationRepeatCount:100];
    [UIView setAnimationRepeatAutoreverses:YES];
    
    if(!blinking) 
    {       
        buttonRecord.alpha = 1;
    } 
    else 
    {        
        buttonRecord.alpha = 0;      
    }
    
    blinking = !blinking;
    [UIView commitAnimations];
}

-(void) showRecordingButton{
    
    buttonRecording.hidden = FALSE;

    buttonRecording.alpha = 0;
    BOOL blinking=NO;
    
    [UIView beginAnimations:@"showingRecording" context:NULL];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationRepeatCount:100];
    [UIView setAnimationRepeatAutoreverses:YES];
    
    if(!blinking) 
    {       
        buttonRecording.alpha = 1;
    } 
    else 
    {        
        buttonRecording.alpha = 0;      
    }
    
    blinking = !blinking;
    [UIView commitAnimations];  
}

-(void) showControlsWhenPlayEnds{
  /*  [CATransaction begin];
    [self.view.layer removeAllAnimations];
    [CATransaction commit];
*/
    [buttonPlay setSelected:FALSE];
    buttonRecording.hidden=TRUE;
    [self showRecordAndPlayButton];
    buttonPrev.hidden=FALSE;
    buttonNext.hidden=FALSE;
    buttonChapter.hidden=FALSE;
    buttonHome.hidden=FALSE;
    [self showArrows:1.5:50];
    
    [UIView beginAnimations:@"showingElements" context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(finishedShowingElements:)];
    buttonRecord.alpha=1;
    [UIView commitAnimations];
}

-(void) showControlsWhenRecordsEnds
{
    [self showRecordAndPlayButton];
    buttonRecording.hidden=TRUE;
    buttonHome.hidden=FALSE;
    buttonChapter.hidden=FALSE;
    buttonNext.hidden=FALSE;
    buttonPrev.hidden=FALSE;
    [UIView beginAnimations:@"showingElements" context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(finishedShowingElements:)];
    buttonRecord.alpha=1;
    [UIView commitAnimations];
}

-(void) showRecordAndPlayButton
{
    buttonRecord.hidden=FALSE;
    buttonPlay.hidden=FALSE;

    buttonRecord.frame = originalRecordFrame;
    buttonPlay.frame = originalPlayFrame;
}

#pragma mark Application Notifications

-(void)applicationDidEnterBackground:(NSNotification*)notification 
{
    if(isRecording){
        [self showControlsWhenRecordsEnds];
        [timer invalidate];
        isRecording=FALSE;
        [controller.voiceRecorder stop];
    }
    [self takeScreenShot];
    [controller stopVideoAndAudio];
}

-(void)applicationDidBecomeActive:(NSNotification*)notification
{
    shouldAnimate=FALSE;
    hasRecorded = NO;
//    [self takeScreenShot];
    [self loadChapter];
    [self initChapter];
}


#pragma mark Delegates

#pragma mark Record Type
-(void) recordTypeClicked:(int)recordType
{
    [selectedTrack setValue:[[NSNumber alloc] initWithInt:recordType] forKey:@"Type"];
    [popupRecordType removeFromSuperview];
    popupRecordType=nil;
//    [self showRecordButton];
    [self initChapter];
}

-(void) recordTypeClose
{
    [popupRecordType removeFromSuperview];
    popupRecordType=nil;
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark Track select


-(void) trackSelectClose
{
    [popupRecordTrackView removeFromSuperview];
    popupRecordTrackView=nil;
    [self.navigationController popViewControllerAnimated:TRUE];

    
}

-(void) trackSelectRecordTypeClicked:(int)trackNumber:(int)recordType
{
    NSLog(@"%@",controller.settings.records);
    selectedTrack=[controller.settings.records objectForKey:[NSString stringWithFormat:@"Track0%d",trackNumber]];
    NSLog(@"%@",[NSString stringWithFormat:@"Track0%d",trackNumber]);

    NSLog(@"%@",selectedTrack);
    if([[selectedTrack objectForKey:@"Saved"]boolValue]==TRUE){
        NSString *trackPath=[model.pathDocuments stringByAppendingPathComponent:[selectedTrack objectForKey:@"Path"]];

        [[NSFileManager defaultManager] removeItemAtPath:trackPath error:nil];
        [selectedTrack setObject:[[NSNumber alloc] initWithBool:FALSE] forKey:@"Saved"];

    }
    [selectedTrack setValue:[[NSNumber alloc] initWithInt:recordType] forKey:@"Type"];
    [popupRecordTrackView removeFromSuperview];
    popupRecordTrackView=nil;

    //[self showRecordButton];
    [self initChapter];
}

-(void) trackSelectClicked:(int)trackNumber
{      
    selectedTrack=[controller.settings.records objectForKey:[NSString stringWithFormat:@"Track0%d",trackNumber]];
    NSString *trackPath=[model.pathDocuments stringByAppendingPathComponent:[selectedTrack objectForKey:@"Path"]];
    
    if([[selectedTrack objectForKey:@"Saved"]boolValue]==FALSE){
        [[NSFileManager defaultManager] removeItemAtPath:trackPath error:nil];
        [selectedTrack setObject:[[NSNumber alloc] initWithBool:FALSE] forKey:@"Saved"];
        [popupRecordTrackView initializeView2];
    }
    else{
        NSLog(@"New track selected %@",selectedTrack);
        [popupRecordTrackView removeFromSuperview];
        popupRecordTrackView=nil;
        [self pressedChapter:self.view];
   
    }
        
}

#pragma mark Record Name

-(void) recordNameClickedClose
{
    [popupRecordName removeFromSuperview];
    popupRecordName=nil;
}

-(void) recordNameClickedOK:(NSString *)name
{
    [selectedTrack setValue:name forKey:@"Title"];
    [selectedTrack setObject:[[NSNumber alloc] initWithBool:TRUE]  forKey:@"Saved"];
    
    [controller.settings saveConfig];
    [controller cleanFileNameToDeleteArray];
    [popupRecordName removeFromSuperview];
    popupRecordName=nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [self.navigationController popViewControllerAnimated:TRUE];
    
}

#pragma mark Chapter 
-(void)chapterClose
{
    auxSwiping=FALSE;
    [popupChapterView removeFromSuperview];
    popupChapterView=nil;
    [self loadChapter];
    [self takeScreenShot];
    [self initChapter];
}

-(void) chapterClicked:(int)chapterIndex
{
    auxSwiping=FALSE;
    [popupChapterView removeFromSuperview];
    popupChapterView=nil;
    [model.book pageAtIndex:chapterIndex];
    shouldAnimate=FALSE;
    [self loadChapter];
    [self takeScreenShot];
    [self initChapter];
}


#pragma mark Actions
- (IBAction)pressedHome:(id)sender
{
    if(hasRecorded && ![[selectedTrack objectForKey:@"Saved"] boolValue]){
        [controller.voiceRecorder stop];
         popupRecordName= [[RecordNameView alloc] initWithFrame:self.view.frame];
         popupRecordName.delegate=self;
        [popupRecordName initializeView:selectedTrack];
        NSLog(@"%@",selectedTrack);
        [self.view addSubview:popupRecordName];
         [popupRecordName playSound];
    }
    else{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil]; 
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil]; 
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

- (IBAction)pressedChapter:(id)sender
{
    auxSwiping=TRUE;
    [timer invalidate];
    popupChapterView=[[ChapterSelectView alloc] initWithFrame:self.view.frame];
    popupChapterView.delegate=self;
    [self.view addSubview:popupChapterView];
    [popupChapterView initializeView];
}

- (IBAction)pressedNextPage:(id)sender {

    NSLog(@"SWIPING %d",auxSwiping);
    if(auxSwiping)
        return;
    
    shouldAnimate=TRUE;
    if([model.book nextPageExist]){
        [self takeScreenShot];
        [model.book nextPage];
        nextDirection=TRUE;
        auxSwiping=TRUE;
        [self loadChapter];
        [self hideControls];
    }
}

- (IBAction)pressedPrevPage:(id)sender {

    NSLog(@"SWIPING %d",auxSwiping);
    if(auxSwiping)
        return;
    shouldAnimate=TRUE;
    if([model.book prevPageExist]){
        [self takeScreenShot];
        [model.book prevPage];
        nextDirection=FALSE;
        auxSwiping=TRUE;
        [self loadChapter];
        [self hideControls];
    }
}

- (IBAction)pressedPlay:(id)sender
{
    if(!isPlaying)
    {
        [self hideControlsWhilePlaying];
//        [self startChapter];
        [self loadWebView];
        isPlaying = YES;
        auxSwiping = TRUE;
    }
    else{
        [controller stopVideoAndAudio];
    }
}

- (IBAction)pressedRecord:(id)sender
{
    isRecording=TRUE;
    
    [self hideControlsWhileRecording];
    [self loadWebView];
    hasRecorded=TRUE;
    auxSwiping=TRUE;
}

- (IBAction)pressedRecording:(id)sender {

    [controller stopVideoAndAudio];
}

#pragma mark Movie notifications

-(void)moviePlayBackDidFinish:(NSNotification*)notification
{
    moviePlayer = [notification object];
    NSLog(@"Playback ended");
    
    auxSwiping=FALSE;
    
    if(isRecording){

        isRecording=FALSE;
        [controller.voiceRecorder stop];
        [self showControlsWhenRecordsEnds];
    }
    else if(isPlaying){
        isPlaying = FALSE;
        [self showControlsWhenPlayEnds];
    }
    else
    {
        NSLog(@"Unknown mode");
    }

    shouldAnimate = NO;
    [self loadChapter];
    [self initChapter];
}

-(void)showRecordView
{
    self.viewRecord.hidden=FALSE;
}

-(void)movieDidFinishLoading:(NSNotification*)notification{

    moviePlayer = [notification object];
    MPMovieLoadState movLoadState = moviePlayer.loadState;
    NSLog(@"movLoadState : %d", (int)movLoadState);
    if(moviePlayer.loadState & MPMovieLoadStatePlaythroughOK || moviePlayer.loadState &MPMovieLoadStatePlayable)
    {
        [self performSelector:@selector(showRecordView)withObject:nil afterDelay:0];
        imageKaraokeBackground.image=[[UIImage alloc]initWithContentsOfFile:[model.pathBook stringByAppendingPathComponent:model.book.page.backName]];

        if(shouldAnimate){
            FTAnimationDirection direction=kFTAnimationLeft;
            if(nextDirection)
                direction=kFTAnimationRight;
            [self.viewRecord slideInFrom:direction   inView:self.view duration:2 delegate:self startSelector:@selector(startAnimation:) stopSelector:@selector(stopAnimation:finished:)];
            shouldAnimate=FALSE;
        }
        else{
            [self loadWebView];
            if(!auxSwiping)
                [self showArrows:1.5 :50];
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:moviePlayer];  
    }
}

#pragma mark Load movie

-(void) initChapter{
    
    if(timer)
        [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.03f target:self selector:@selector(update:) userInfo:nil repeats:YES];
    
    seconds=0;
    
    BOOL recordOnly = YES;
    if([[controller getTrackPathArray:[model.book getCurrentIndex] forTrack:selectedTrack] count] > 0)
    {
        [self showRecordAndPlayButton];
        recordOnly = NO;
    }
    else
    {
        buttonPlay.hidden = TRUE;
    }
    
    auxSwiping = NO;
    [self showArrows:1.5 :50];
    [self showRecordButton:recordOnly];
    buttonHome.hidden = NO;
    buttonChapter.hidden = NO;
    
}


-(void)takeScreenShot
{
    CGFloat playBackTime = moviePlayer.currentPlaybackTime;
    if(playBackTime >= 0)
    {
        UIImage *auxImage = [moviePlayer thumbnailImageAtTime:0 timeOption:MPMovieTimeOptionExact];
        if(auxImage)
        {
            imageVideoScreenshot.image = auxImage;
//            NSLog(@"new screenshot at time : %f", playBackTime);
        }
    }
}


-(void) loadChapter{


    // Chargment du flim
    [controller loadSelectedChapter:nil ofType:0];

    // Screenshot pour faire plus joli
    viewScreenshot.backgroundColor = [UIColor clearColor];
    [viewScreenshot bringSubviewToFront:imageScreenshot];
    
    UIGraphicsBeginImageContext(self.viewKaraoke.bounds.size);
    [self.viewKaraoke.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    imageScreenshot.image = img;
    
/*    UIImage *auxImage = [moviePlayer thumbnailImageAtTime:moviePlayer.currentPlaybackTime timeOption:MPMovieTimeOptionExact];
    if(auxImage)
    {
        imageVideoScreenshot.image=auxImage;
    }
 */
    self.viewRecord.hidden = TRUE;
    webViewText.hidden=TRUE;
 

    // Remove des anim
    [CATransaction begin];
    [self.view.layer removeAllAnimations];
    [CATransaction commit];
    [buttonChapter setTitle:[NSString stringWithFormat:@"%d",[model.book getCurrentIndex] + 1] forState:UIControlStateNormal];

    
    // Notif
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidFinishLoading:) name:MPMoviePlayerLoadStateDidChangeNotification  object:moviePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
}

-(void) startChapter{
    
    [buttonChapter setTitle:[NSString stringWithFormat:@"%d",[model.book getCurrentIndex]+1] forState:UIControlStateNormal];
    [moviePlayer setCurrentPlaybackTime:0.0];
    

    NSURLRequest *request=[[NSURLRequest alloc]initWithURL:[[NSURL alloc] initFileURLWithPath:[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]]];
    NSLog(@"%@",[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]);
    [webViewText loadRequest:request];

    NSArray* trackPathArray = [controller getTrackPathArray:[model.book getCurrentIndex] forTrack:selectedTrack];
    int type=[[selectedTrack objectForKey:@"Type"] intValue];
    [controller replaceAudioFileOfType:type withFiles:trackPathArray];
    [controller playSelectedChapterOfType:type];
}

#pragma mark webview


-(void) loadWebView
{
    [webViewText stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML = \"\";"];
    
    NSURLRequest *request=[[NSURLRequest alloc]initWithURL:[[NSURL alloc] initFileURLWithPath:[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]]];
    NSLog(@"%@",[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]);
    [webViewText loadRequest:request];
}


-(void) webViewDidStartLoad:(UIWebView *)webView
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSString* recordsArray=@"";

    for(LrcRecord* record in model.subtitles){
        NSString *aux=[self cleanString:record.text];
        recordsArray=[recordsArray stringByAppendingFormat:@"{text:'%@',start:%.2f, end:%.2f}",aux,record.timeBegin,record.timeEnd];
        if(record!=[model.subtitles objectAtIndex:model.subtitles.count-1])
            recordsArray=[recordsArray stringByAppendingString:@","];     
    }

    NSString *function;
 
    
    if(isPlaying || isRecording)
        function=[NSString stringWithFormat:@"initializeText(new Array(%@),%f,%f,'%@','%@',1,'%@')",recordsArray,self.webViewText.frame.size.width,self.webViewText.frame.size.height,model.book.page.karaokeColor, model.book.page.karaokeNormalcolor, [LanguageModel localeId]];
    else
        function=[NSString stringWithFormat:@"initializeText(new Array(%@),%f,%f,'%@','%@',0, '%@')",recordsArray,self.webViewText.frame.size.width,self.webViewText.frame.size.height,model.book.page.karaokeColor, model.book.page.karaokeNormalcolor, [LanguageModel localeId]];
    
    NSLog(@"java function: %@",function);
    [self.webViewText stringByEvaluatingJavaScriptFromString:function];
    if(isRecording || isPlaying)
    {
        int type=[[selectedTrack objectForKey:@"Type"] intValue];
        if(isRecording)
        {
            [controller recordChapterOfType:type forTrack:selectedTrack];
        }
        else
        {
            NSArray* trackPathArray = [controller getTrackPathArray:[model.book getCurrentIndex] forTrack:selectedTrack];
            [controller replaceAudioFileOfType:type withFiles:trackPathArray];
            [controller playSelectedChapterOfType:type];
        }
        auxSwiping=TRUE;
    }
    webViewText.hidden=FALSE;
}

-(NSString*)cleanString:(NSString*) string{
    string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\""];
    string=[string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    string=[string stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
    string=[string stringByReplacingOccurrencesOfString:@"'" withString:@"\'"];
    return string;
}



-(void)update:(NSTimer*)timer_{
    seconds+=0.03;
    if(seconds>ARROWS_TIME){
        buttonNext.hidden=TRUE;
        buttonPrev.hidden=TRUE;
        [timer invalidate];
    }
    
}




@end
