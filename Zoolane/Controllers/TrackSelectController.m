//
//  TrackSelectController.m
//  Zoolane
//
//  Created by Developer IMX on 9/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TrackSelectController.h"
#import "AppDelegate.h"
#import "ApplicationController.h"
#import "SoftwareModel.h"
#import "LanguageModel.h"
#import "AudioPlayer.h"

@interface TrackSelectController ()

@end

@implementation TrackSelectController
@synthesize labelTitle;
@synthesize buttonTrack1;
@synthesize buttonTrack2;
@synthesize buttonTrack3;
@synthesize buttonNewTrack;
@synthesize playMode;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;

    if(INTERFACE_IS_PHONE)
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE_IPHONE]];
    else {
        [labelTitle setFont:[UIFont fontWithName:SYSTEM_FONT size:FONT_SIZE]];
    }
    
    audioPlayer=[[AudioPlayer alloc]init];
    NSString *audioFile=[model.pathSource stringByAppendingPathComponent:@"Sounds/tutorial"];
    audioFile=model.pathSource;
    audioFile=[[audioFile stringByAppendingPathComponent:[LanguageModel key2String:@"TUTORIAL_CHOOSE_STORY"]] stringByAppendingString:@".aif"];
    
    NSLog(@"%@",audioFile);
    
    [audioPlayer preload:[[NSURL alloc] initFileURLWithPath:audioFile]];
    

	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setLabelTitle:nil];
    [self setButtonTrack1:nil];
    [self setButtonTrack2:nil];
    [self setButtonTrack3:nil];
    [self setButtonNewTrack:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) viewWillAppear:(BOOL)animated {

    if(playMode)
        labelTitle.text=[LanguageModel key2String:@"PLAY_TRACK_CAPTION"];
    else
        labelTitle.text=[LanguageModel key2String:@"RECORD_TRACK_CAPTION"];
    
    [buttonTrack1 setTitle:[LanguageModel key2String:@"TRACK_ONE"] forState:UIControlStateNormal];
    [buttonTrack2 setTitle:[LanguageModel key2String:@"TRACK_TWO"] forState:UIControlStateNormal];
    [buttonTrack3 setTitle:[LanguageModel key2String:@"TRACK_THREE"] forState:UIControlStateNormal];
    
    if(!playMode)
        [buttonNewTrack setTitle:[LanguageModel key2String:@"RECORD_NEW_STORY"] forState:UIControlStateNormal];
    else 
        [buttonNewTrack setTitle:[LanguageModel key2String:@"ORIGINAL_TRACK"] forState:UIControlStateNormal];
    
    NSString *track1Title=[[controller.settings.records objectForKey:@"Track01"] objectForKey:@"Title"];
    NSString *track2Title=[[controller.settings.records objectForKey:@"Track02"] objectForKey:@"Title"];
    NSString *track3Title=[[controller.settings.records objectForKey:@"Track03"] objectForKey:@"Title"];

   if(![track1Title isEqualToString:@""])
       [buttonTrack1 setTitle:track1Title forState:UIControlStateNormal];
    
    if(![track2Title isEqualToString:@""])
        [buttonTrack2 setTitle:track2Title forState:UIControlStateNormal];
    
    if(![track3Title isEqualToString:@""])
        [buttonTrack3 setTitle:track3Title forState:UIControlStateNormal];

}

-(void) viewDidAppear:(BOOL)animated{
    [audioPlayer play];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)pressedTrack1:(id)sender {
    [self dismissModalViewControllerAnimated:TRUE];
    [self.delegate clickedTrackSelect:1:deleteTrack];
}

- (IBAction)pressedTrack2:(id)sender {
    [self dismissModalViewControllerAnimated:TRUE];
    [self.delegate clickedTrackSelect:2:deleteTrack];
}
- (IBAction)pressedTrack3:(id)sender {
    [self dismissModalViewControllerAnimated:TRUE];
    [self.delegate clickedTrackSelect:3:deleteTrack];

}

- (IBAction)pressedNewTrack:(id)sender {
    if(playMode){
        [self.delegate clickedTrackSelect:0:FALSE];
    }
    else{
        labelTitle.text=[LanguageModel key2String:@"RECORD_CREATE_NEW_TRACK_FAIL"];
        buttonNewTrack.hidden=TRUE;
        deleteTrack=TRUE;
    }
    
}
@end
