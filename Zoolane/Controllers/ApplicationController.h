//
//  ApplicationController.h
//  Zoolane
//
//  Created by Developer IMX on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoftwareModel.h"
#import "SettingsModel.h"
#import "VoiceRecorder.h"
#import "Facebook.h"

@interface ApplicationController : NSObject
{
    BOOL isPaused;
    NSMutableArray* fileNameToDeleteArray;
}

@property(strong, nonatomic) SoftwareModel *model;
@property(strong, nonatomic) SettingsModel *settings;
@property(strong, nonatomic) VoiceRecorder *voiceRecorder;


+(BOOL) isNetworkAvailable:(BOOL)Wifi:(BOOL)WWan;

+ (BOOL)validateEmailWithString:(NSString*)email;

-(void) pauseVideoAndAudio;

-(void) resumeVideoAndAudio;

-(void)pauseResumeVideoAndAudio;

-(void) stopVideoAndAudio;

-(NSString*) loadSelectedChapter:(NSString*) recordFile ofType:(int)type;

-(void) replaceAudioFileOfType:(int)type withFiles:(NSArray*)recordFile;

-(void) playSelectedChapterOfType:(int)type;


-(BOOL) anyTrackRecorded;
-(BOOL) allTracksRecorded;

-(NSArray*) getTrackPathArray:(int) indexChapter forTrack:(NSMutableDictionary*)track;
-(void) recordChapterOfType:(int)type forTrack:(NSMutableDictionary*)track;

-(void)cleanFileNameToDeleteArray;
-(void)deleteFileInFileNameToDeleteArray;

@end
