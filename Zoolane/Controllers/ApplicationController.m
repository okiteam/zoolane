//
//  ApplicationController.m
//  Zoolane
//
//  Created by Developer IMX on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ApplicationController.h"
#import "Reachability.h"
#import "Page.h"
#import "BookModel.h"
#import "LrcRecord.h"
#import "AudioPlayer.h"
#import "AppDelegate.h"

@interface ApplicationController ()

@end

@implementation ApplicationController
@synthesize model;
//@synthesize permissions;
@synthesize settings;
@synthesize voiceRecorder;

-(id) init{
    
    model=[[SoftwareModel alloc] init];
    settings=[[SettingsModel alloc] init];
    [settings loadConfig];
    isPaused = NO;
    fileNameToDeleteArray = [[NSMutableArray alloc] init];
    
    // FB
//    permissions = [[NSArray alloc] initWithObjects:@"offline_access", nil];
    
    return self;
}

+(BOOL) isNetworkAvailable:(BOOL)Wifi:(BOOL)WWan{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
	NetworkStatus internetStatus = [r currentReachabilityStatus];
	BOOL internet=NO;
    if(Wifi && internetStatus==ReachableViaWiFi){
        internet=YES;
    }
    if(WWan && internetStatus==ReachableViaWWAN){
        internet=YES;
    }
    return internet;
    
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    return [emailTest evaluateWithObject:email];
}

-(void) pauseVideoAndAudio
{
    [model.narPLayer pause];
    [model.diaPLayer pause];
    for (AudioPlayer* ap in model.audioPlayerArray) {
        [ap pause];
    }
    
    [model.sfx pause];
    [model.music pause];   
    [model.moviePlayer pause];
    isPaused = YES;
}

-(void) resumeVideoAndAudio
{
    [model.narPLayer resume];
    [model.diaPLayer resume];
    for (AudioPlayer* ap in model.audioPlayerArray) {
        [ap resume];
    }

    [model.sfx resume];
    [model.music resume];
    [model.moviePlayer pause];
    isPaused = NO;
}


-(void)pauseResumeVideoAndAudio{
    if(isPaused)
    {
        [self resumeVideoAndAudio];
    }
    else
    {
        [self pauseVideoAndAudio];
    }
}

-(void) stopVideoAndAudio
{
    isPaused = NO;

    [model.music stop];
    [model.narPLayer stop];
    [model.diaPLayer stop];
    for (AudioPlayer* ap in model.audioPlayerArray) {
        [ap stop];
    }

    [model.sfx stop];
    [model.moviePlayer stop];
}



-(NSString*) loadSelectedChapter:(NSString*) recordFile ofType:(int)type{

    NSString *auxText;
    
    NSString *videoFile=[model.pathBook stringByAppendingPathComponent:model.book.page.videoName];
    NSLog(@"videoFile : %@",videoFile);

    
    NSURL *urlOfMovie = [NSURL fileURLWithPath:videoFile];
    [model.moviePlayer setContentURL:urlOfMovie];
    [model.moviePlayer prepareToPlay];
    
    
    NSString *audioFile=[model.pathBook stringByAppendingPathComponent:model.book.page.musicName];
    [model.music preloadWithFullPath:audioFile];
    NSLog(@"audioFile : %@",audioFile);
    
    if(recordFile!=nil && (type==0 || type==1)){
        [model.narPLayer preloadWithFullPath:recordFile];
    }
    else{
        audioFile=[model.pathBook stringByAppendingPathComponent:model.book.page.voiceNarratorName];
        audioFile=[audioFile stringByReplacingOccurrencesOfString:@"LANGCODE" withString:[LanguageModel localeId]];
        [model.narPLayer preloadWithFullPath:audioFile];
        NSLog(@"narr file: %@",audioFile);
    }
    
    if(recordFile!=nil && type==2){
        [model.diaPLayer preloadWithFullPath:recordFile];
    }
    else{
        audioFile=[model.pathBook stringByAppendingPathComponent:model.book.page.voiceConversationName];
        audioFile=[audioFile stringByReplacingOccurrencesOfString:@"LANGCODE" withString:[LanguageModel localeId]];
        
        [model.diaPLayer preloadWithFullPath:audioFile];
        NSLog(@"conv file: %@",audioFile);
        
    }
    
    audioFile=[model.pathBook stringByAppendingPathComponent:model.book.page.sfxName];
    [model.sfx preloadWithFullPath:audioFile];
    NSLog(@"sfx file :%@",audioFile);
    
    [model.subtitles removeAllObjects];
    NSString *lrcFile=[model.pathBook stringByAppendingPathComponent:model.book.page.lrcConversationName];
    
    lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"LANGCODE" withString:[LanguageModel localeId]];
    //NSLog(@"%@",[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@"_iphone"]);
    if(INTERFACE_IS_PHONE && [[NSFileManager defaultManager] fileExistsAtPath:[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@"_iphone"]] )
        lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@"_iphone"];
    else
        lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@""];

    
    NSLog(@"conv lrcFile : %@",lrcFile);
    
    NSMutableArray *array=[LrcRecord readLrcsFromFile:lrcFile];
    for(LrcRecord* record in array){
        [model.subtitles addObject:record];

    }
    
    lrcFile=[model.pathBook stringByAppendingPathComponent:model.book.page.lrcNarratorName];
    lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"LANGCODE" withString:[LanguageModel localeId]];
    
    if(INTERFACE_IS_PHONE && [[NSFileManager defaultManager] fileExistsAtPath:[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@"_iphone"]] )
        lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@"_iphone"];
    else
        lrcFile=[lrcFile stringByReplacingOccurrencesOfString:@"_DEVICE" withString:@""];

    NSLog(@"narr lrcFile : %@",lrcFile);
    array=[LrcRecord readLrcsFromFile:lrcFile];
    for(LrcRecord* record in array){
        [model.subtitles addObject:record];
    }
    
    model.subtitles=[[model.subtitles sortedArrayUsingComparator:^(id a, id b) {
        LrcRecord *first=(LrcRecord*)a;
        LrcRecord *second=(LrcRecord*)b;
        
        if(first.timeBegin==second.timeBegin)
            return NSOrderedSame;
         
        return (first.timeBegin<second.timeBegin) ? NSOrderedAscending:NSOrderedDescending;

     }] mutableCopy];

    auxText=@"";
    for(LrcRecord* record in model.subtitles){
         NSLog(@"%f %f %@",record.timeBegin,record.timeEnd, record.text);
        auxText=[auxText stringByAppendingFormat:@"%@\n",record.text];
        
                 
    }    
                 
    model.book.page.textContent=auxText;
    return auxText;

}


-(void) replaceAudioFileOfType:(int)type withFiles:(NSArray*)recordFile
{
    [model.audioPlayerArray removeAllObjects];
    
    if([recordFile count] > 0)
    {
        switch (type) {
                // whole
            case 0:
                [model.diaPLayer preloadWithFullPath:nil];
                [model.narPLayer preloadWithFullPath:nil];
                break;
                // nar only
            case 1:
                [model.narPLayer preloadWithFullPath:nil];
                break;
                // dia only
            case 2:
                [model.diaPLayer preloadWithFullPath:nil];
                break;
            default:
                break;
        }
        
        for (NSString* audioPath in recordFile)
        {
            AudioPlayer* av = [[AudioPlayer alloc] init];
            [av preloadWithFullPath:audioPath];
            [model.audioPlayerArray addObject:av];
        }
    }
}


-(void) playSelectedChapterOfType:(int)type{
    
    // Find timing
    NSArray* recordingArray = nil;
    if(type > 0)
    {
        NSDictionary* alltimingDic = type == 1 ? model.allTimingNarDic : model.allTimingDiaDic;
        NSString* key = [NSString stringWithFormat:@"%d", ([model.book getCurrentIndex] + 1)];
        recordingArray = [alltimingDic objectForKey:key];
    }
    int index = 0;
    for (AudioPlayer* ap in model.audioPlayerArray)
    {
        CGFloat start = 0.f;
        if (index < [recordingArray count]) {
            start = [[[recordingArray objectAtIndex:index] objectForKey:@"start"] floatValue];
        }
        else
            NSLog(@"Erreur dans les timing de lecture !!");
        [ap playAtTime:start];
        index++;
    }
    
    // play
    [model.moviePlayer play];
    [model.music play];
    [model.sfx play];
    
    [model.diaPLayer play];
    [model.narPLayer play];
    isPaused = NO;
}


-(BOOL) anyTrackRecorded{
    for(NSString *dic in settings.records){
        NSDictionary *aux=[settings.records objectForKey:dic];
        
        if([[aux valueForKey:@"Saved"] boolValue]){
            return TRUE;
        }
    }
    return FALSE;
}

-(BOOL) allTracksRecorded{
    for(NSString *dic in settings.records){
        NSDictionary *aux=[settings.records objectForKey:dic];
        
        if(![[aux valueForKey:@"Saved"] boolValue]){
            return FALSE;
        }
    }
    return TRUE;
}

-(NSArray*) getTrackPathArray:(int)indexChapter forTrack:(NSMutableDictionary*)track
{
    NSMutableArray* trackPathArray = nil;
    NSString *trackPath = [model.pathDocuments stringByAppendingPathComponent:[track objectForKey:@"Path"]];
    NSArray* fileArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:trackPath error:nil];
    if([fileArray count] > 0)
    {
        trackPathArray = [[NSMutableArray alloc] init];
        NSString* fileNameToCompare = [NSString stringWithFormat:@"%@_%@",[track objectForKey:@"Path"], [model.book getStringCurrentIndex]];
        for (NSString* fileName in fileArray) {
            if([fileName hasPrefix:fileNameToCompare])
            {
                NSString* filePath = [trackPath stringByAppendingPathComponent:fileName];
                [trackPathArray addObject:filePath];
            }
        }
    }
    return (NSArray*)trackPathArray;
}


-(void) recordChapterOfType:(int)type forTrack:(NSMutableDictionary*)track// andRecordingData:(NSArray*)recordingArray
{
    // RECORD
    if(!voiceRecorder)
        voiceRecorder = [[VoiceRecorder alloc] init];
    
    NSString* saveName = [track objectForKey:@"Path"];
    NSString *recordFile = [model.pathDocuments stringByAppendingPathComponent:saveName];
    BOOL isDir;
    if(![[NSFileManager defaultManager] fileExistsAtPath:recordFile isDirectory:&isDir]){
        [[NSFileManager defaultManager] createDirectoryAtPath:recordFile withIntermediateDirectories:TRUE attributes:nil error:nil];
    }
    // Delete old file(s)
    NSArray* fileArray = [self getTrackPathArray:type forTrack:track];
    if([fileArray count] > 0)
    {
        for (NSString* fileName in fileArray) {
            if(![[NSFileManager defaultManager] removeItemAtPath:fileName error:nil])
                NSLog(@"failed to delete file");
        }
    }
    
    // Find timing
    NSArray* recordingArray = nil;
    NSInteger indexChapter = [model.book getCurrentIndex];
    NSString* indexChapterStr = [model.book getStringCurrentIndex];
    if(type > 0)
    {
        NSDictionary* alltimingDic = type == 1 ? model.allTimingNarDic : model.allTimingDiaDic;
        recordingArray = [alltimingDic objectForKey:[NSString stringWithFormat:@"%d", (indexChapter + 1)]];
        if(!recordingArray)
            NSLog(@"Erreur : pas de timing d'enregistrement, on enregistre tout");
    }
    
    
    // Launch record
    if(!recordingArray) // ie type == 0
    {
        recordFile = [recordFile stringByAppendingFormat:@"/%@_%@_0.caf", saveName, indexChapterStr];
        [fileNameToDeleteArray addObject:recordFile];
        [voiceRecorder addRecordToFile:recordFile withTimingDic:nil];
    }
    else
    {
        NSInteger fileIndex = 0;
        for (NSDictionary* timingDic in recordingArray)
        {
            NSString* recordFileName = [recordFile stringByAppendingFormat:@"/%@_%@_%d.caf", saveName, indexChapterStr, fileIndex];
            [fileNameToDeleteArray addObject:recordFileName];
            [voiceRecorder addRecordToFile:recordFileName withTimingDic:timingDic];
            fileIndex++;
        }
    }
    [voiceRecorder startRecording];
    
    
    // PLAY
    if(type == 1)
        [model.diaPLayer play];
    else if(type == 2)
        [model.narPLayer play];
    
    [model.moviePlayer play];
    isPaused = NO;
}


-(void)cleanFileNameToDeleteArray
{
    [fileNameToDeleteArray removeAllObjects];
}

-(void)deleteFileInFileNameToDeleteArray
{
    for (NSString* fileName in fileNameToDeleteArray) {
        if(![[NSFileManager defaultManager] removeItemAtPath:fileName error:nil])
            NSLog(@"failed to delete file");
    }
    [fileNameToDeleteArray removeAllObjects];
}

@end
