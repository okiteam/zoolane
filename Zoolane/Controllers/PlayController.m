//
//  PlayController.m
//  Zoolane
//
//  Created by Developer IMX on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayController.h"
#import "Page.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import "AudioPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "LrcRecord.h"
#import "FTAnimation.h"
#import "FTAnimationManager.h"
#import "FPPopoverController.h"

#import <Foundation/NSAttributedString.h>
#import <CoreText/CoreText.h>
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioServices.h>

#import "TrackSelectController.h"

@interface PlayController ()

@end

@implementation PlayController
@synthesize webviewText;
@synthesize buttonChapter;
@synthesize buttonNextPage;
@synthesize buttonPrevPage;
@synthesize buttonHome;
@synthesize imageBackground;
@synthesize karaokeView;
@synthesize viewScreenshot;
@synthesize imageScreenshot;
@synthesize imageKaraokeScreenshot;
@synthesize recordView;
@synthesize videoView;
@synthesize buttonMute;

#pragma mark View Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];

    controller=((AppDelegate * )[[UIApplication sharedApplication] delegate]).controller;
    model=controller.model;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

    // Gesture recognizers
    UISwipeGestureRecognizer *swipeGestureRight=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(prevChapter:)];
    UISwipeGestureRecognizer *swipeGestureLeft=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextChapter:)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    swipeGestureLeft.cancelsTouchesInView=TRUE;
    swipeGestureRight.cancelsTouchesInView=TRUE;
    [self.view addGestureRecognizer:swipeGestureRight];
    [self.view addGestureRecognizer:swipeGestureLeft];

    // Audio+video
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
  //  UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
//    AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute,sizeof(audioRouteOverride),&audioRouteOverride);
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof (doChangeDefaultRoute), &doChangeDefaultRoute);
    [session setActive:YES error:nil];
    
    moviePlayer=model.moviePlayer;
    [self.videoView addSubview:moviePlayer.view];
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = NO;  
    moviePlayer.view.userInteractionEnabled = NO;
    [moviePlayer.view setFrame:videoView.bounds];
    moviePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    moviePlayer.view.backgroundColor = [UIColor blackColor];
    moviePlayer.scalingMode = MPMovieScalingModeAspectFill ;

    // Webview
    webviewText.backgroundColor=[UIColor clearColor];
    [webviewText setOpaque:FALSE];
    webviewText.delegate=self;
    [(UIScrollView *) [webviewText.subviews objectAtIndex:0] setAlwaysBounceVertical:NO];
    [(UIScrollView *) [webviewText.subviews objectAtIndex:0] setAlwaysBounceHorizontal:NO];
    webviewText.scrollView.scrollEnabled=NO;
    webviewText.userInteractionEnabled=FALSE;
    for (id subview in webviewText.subviews){
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    }

    // Other
    buttonNextPage.hidden=TRUE;
    buttonPrevPage.hidden=TRUE;
    shouldAnimate=FALSE;
    
    // Load first chapter
    [model.book firstPage];
    [self loadChapter];
    [self takeScreenShot];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    startPlaying = NO;
}


-(void) viewDidAppear:(BOOL)animated
{
    if([controller anyTrackRecorded]){
        popupPlayView=[[PlaySelectTrackView alloc] initWithFrame:self.view.frame];
        popupPlayView.delegate=self;
        [self.view addSubview:popupPlayView];
        [popupPlayView initializeView];
        [popupPlayView playSound];
        startPlaying = NO;
        auxSwiping = YES;
    }
    else {
        auxSwiping = NO;
        startPlaying = YES;
        [self loadChapter];
    }
}

- (void)viewDidUnload
{
    [self setVideoView:nil];
    [self setButtonChapter:nil];
    [self setButtonNextPage:nil];
    [self setButtonPrevPage:nil];
    [self setImageBackground:nil];
    [self setKaraokeView:nil];
    [self setViewScreenshot:nil];
    [self setImageScreenshot:nil];
    [self setImageKaraokeScreenshot:nil];
    [self setRecordView:nil];
    [self setWebviewText:nil];
    [self setButtonHome:nil];
    [self setButtonMute:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation==UIInterfaceOrientationLandscapeLeft);
}

#pragma mark Animation events

-(void) startAnimation:(CAAnimation *)anim{
    
}

-(void) stopAnimation:(CAAnimation *)anim finished:(BOOL)flag{

    [self startChapter];
}

#pragma mark Application Notifications

-(void)applicationDidEnterBackground:(NSNotification*)notification 
{
    [controller pauseVideoAndAudio];
}

-(void)applicationDidBecomeActive:(NSNotification*)notification 
{
    shouldAnimate=FALSE;
    [self loadChapter];
}

-(void)showRecordView{
    self.recordView.hidden=FALSE;
}

#pragma mark Movie Notifications

-(void)movieDidFinishLoading:(NSNotification*)notification
{
    moviePlayer = [notification object];
    if(moviePlayer.loadState & MPMovieLoadStatePlaythroughOK || moviePlayer.loadState &MPMovieLoadStatePlayable)
    {
        [self performSelector:@selector(showRecordView)withObject:nil afterDelay:0];
        imageBackground.image=[[UIImage alloc]initWithContentsOfFile:[model.pathBook stringByAppendingPathComponent:model.book.page.backName]];

        if(shouldAnimate)
        {
            FTAnimationDirection direction=kFTAnimationLeft;
            if(nextDirection)
                direction=kFTAnimationRight;
            [self.recordView slideInFrom:direction   inView:self.view duration:2 delegate:self startSelector:@selector(startAnimation:) stopSelector:@selector(stopAnimation:finished:)];
        }
        else if(startPlaying)
        {
            [self startChapter];
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:moviePlayer];  
    }
}


-(void)moviePlayBackDidFinish:(NSNotification*)notification 
{  
    NSLog(@"Movie playback end");    
    moviePlayer = [notification object];  
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];  
    [timer invalidate];
    [self showArrows:1.5 :50];
}  


#pragma mark Actions
- (IBAction)chapterPressed:(id)sender {

    auxSwiping=TRUE;
    [timer invalidate];
    [controller pauseVideoAndAudio];
    popupChapterView=[[ChapterSelectView alloc] initWithFrame:self.view.frame];
    popupChapterView.delegate=self;
    [self.view addSubview:popupChapterView];
    [popupChapterView initializeView];        
}

- (IBAction)backPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil]; 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil]; 
    [timer invalidate];
    [controller stopVideoAndAudio];
    [self.navigationController popViewControllerAnimated:TRUE];
}


- (IBAction)nextChapter:(id)sender
{
    if(auxSwiping){
        NSLog(@"No swiping");
        return;
    }
    
    if([model.book nextPageExist]){
        [controller pauseVideoAndAudio];
        [self takeScreenShot];
        [model.book nextPage];
        auxSwiping=TRUE;
        nextDirection=TRUE;
        shouldAnimate=TRUE;
       [self hideControls];
       [self loadChapter];
    }
}

- (IBAction)prevChapter:(id)sender
{
    if(auxSwiping){
        NSLog(@"No swiping");
    }
    else
    {
        [self takeScreenShot];
        if([model.book prevPageExist] && seconds <= PREVPAGE_TIME){
            [controller pauseVideoAndAudio];
            [model.book prevPage];
            auxSwiping=TRUE;
            nextDirection=FALSE;
            shouldAnimate=TRUE;
        }
        else{
            shouldAnimate=FALSE;
        }
        [self hideControls];
        [self loadChapter];
    }
    
}

- (IBAction)mutePressed:(id)sender
{
    if(model.narPLayer.volume != 0)
    {
        [model.narPLayer setVolume:0.0];
        [model.diaPLayer setVolume:0.0];
        [model.sfx setVolume:0.0];
        [model.music setVolume:0.0];
        for (AudioPlayer* ap in model.audioPlayerArray) {
            [ap setVolume:0.0];
        }
    }
    else
    {
        [model.narPLayer setVolume:1.0];
        [model.diaPLayer setVolume:1.0];
        [model.sfx setVolume:1.0];
        [model.music setVolume:1.0];
        for (AudioPlayer* ap in model.audioPlayerArray) {
            [ap setVolume:1.0];
        }
    }
}



#pragma mark Movie commands

-(void)hideControls
{
    buttonPrevPage.alpha=0;
    buttonNextPage.alpha=0;
    buttonNextPage.hidden=TRUE;
    buttonPrevPage.hidden=TRUE;
    
    buttonMute.hidden=TRUE;
    buttonHome.hidden=TRUE;
    buttonChapter.hidden=TRUE;
}


-(void)takeScreenShot
{
    CGFloat playBackTime = moviePlayer.currentPlaybackTime;
    if(playBackTime >= 0)
    {
        UIImage *auxImage = [moviePlayer thumbnailImageAtTime:playBackTime timeOption:MPMovieTimeOptionExact];
        if(auxImage)
        {
            imageScreenshot.image = auxImage;
            NSLog(@"new screenshot at time : %f", playBackTime);
        }
    }
}

-(void) loadChapter{
  
    [controller loadSelectedChapter:nil ofType:0];
    [self hideControls];

    
    // Screenshot pour faire plus joli
    viewScreenshot.backgroundColor = [UIColor clearColor];
    [viewScreenshot bringSubviewToFront:imageKaraokeScreenshot];
    
    UIGraphicsBeginImageContext(self.karaokeView.bounds.size);
    [self.karaokeView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    imageKaraokeScreenshot.image = img;
    
    self.recordView.hidden = TRUE;
    webviewText.hidden = TRUE;

    // Remove des anim
    [CATransaction begin];
    [self.view.layer removeAllAnimations];
    [CATransaction commit];
    
    // Notif
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidFinishLoading:) name:MPMoviePlayerLoadStateDidChangeNotification  object:moviePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    
}

-(void) startChapter{
    
    buttonChapter.hidden=FALSE;
    buttonHome.hidden=FALSE;
    buttonMute.hidden=FALSE;
    
    [buttonChapter setTitle:[NSString stringWithFormat:@"%d",[model.book getCurrentIndex]+1] forState:UIControlStateNormal];
    NSLog(@"%@",[model.pathBook stringByAppendingPathComponent:model.book.page.backName]);
    
    if(timer)
        [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.03f target:self selector:@selector(update:) userInfo:nil repeats:YES];
    seconds=0;
    
    // Remove old text
    [webviewText stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML = \"\";"];
    
    NSURLRequest *request=[[NSURLRequest alloc]initWithURL:[[NSURL alloc] initFileURLWithPath:[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]]];
    NSLog(@"%@",[model.pathSource stringByAppendingPathComponent:@"Web/textview.xhtml"]);
    [webviewText loadRequest:request];
    NSLog(@"Movie start");
}

-(void) webViewDidStartLoad:(UIWebView *)webView
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return TRUE;
}

-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSString* recordsArray=@"";
    for(LrcRecord* record in model.subtitles){
        NSString *aux=[self cleanString:record.text];
//        NSLog(@"cleaned text: %@", aux);
        recordsArray=[recordsArray stringByAppendingFormat:@"{text:'%@',start:%.2f, end:%.2f}",aux,record.timeBegin,record.timeEnd];
        if(record!=[model.subtitles objectAtIndex:model.subtitles.count-1])
            recordsArray=[recordsArray stringByAppendingString:@","];     
    }
    
    NSString *function=[NSString stringWithFormat:@"initializeText(new Array(%@),%f,%f,'%@','%@',1, '%@')",recordsArray,self.webviewText.frame.size.width,self.webviewText.frame.size.height,model.book.page.karaokeColor, model.book.page.karaokeNormalcolor, [LanguageModel localeId]];
    
    NSLog(@"%@",function);
    [self.webviewText stringByEvaluatingJavaScriptFromString:function ];

    webviewText.hidden=FALSE;

    int type = 0;
    if(selectedTrack){
        NSArray *trackPathArray = [controller getTrackPathArray:[model.book getCurrentIndex] forTrack:selectedTrack];
//        if([trackPathArray count] > 0)
//        {
            type = [[selectedTrack objectForKey:@"Type"]intValue];
            [controller replaceAudioFileOfType:type withFiles:trackPathArray];
//        }
    }
    [controller playSelectedChapterOfType:type];

    [self showArrows:1.5:50];
    auxSwiping=FALSE;
}

-(NSString*)cleanString:(NSString*) string
{
    string=[string stringByReplacingOccurrencesOfString:@"\"" withString:@"\""];
    string=[string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    string=[string stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
    string=[string stringByReplacingOccurrencesOfString:@"'" withString:@"\'"];
    return string;
}

#pragma mark Timer

-(void)update:(NSTimer*)timer
{
    seconds+=0.03;
    if(seconds > ARROWS_TIME){
        buttonNextPage.hidden=TRUE;
        buttonPrevPage.hidden=TRUE;
    }       
}

#pragma mark Arrows

-(void)showArrows:(float) duration:(int)repeatCount
{
    NSLog(@"Showing arrows");
    BOOL blinking = NO;
    if([model.book nextPageExist])
        buttonNextPage.hidden=FALSE;
    else {
        buttonNextPage.hidden=TRUE;
    }
    if([model.book prevPageExist])
        buttonPrevPage.hidden=FALSE;
    else {
        buttonPrevPage.hidden=TRUE;
    }
    
    NSLog(@"%d %d",buttonPrevPage.hidden,buttonNextPage.hidden);
    buttonNextPage.alpha=0;
    buttonPrevPage.alpha=0;
    
    [UIView beginAnimations:@"showingArrows" context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationRepeatCount:repeatCount];
    [UIView setAnimationRepeatAutoreverses:YES];
    
    if(!blinking) 
    {       
        if([model.book nextPageExist])
            self.buttonNextPage.alpha = 1;
        if([model.book prevPageExist])
            self.buttonPrevPage.alpha = 1;        
    } 
    else 
    {        
        if([model.book nextPageExist])
            self.buttonNextPage.alpha = 0;
        if([model.book prevPageExist])
            self.buttonPrevPage.alpha = 0;
    }
    
    blinking = !blinking;
    
    [UIView commitAnimations];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segueRecordToSelectTrackIdentifier"] && INTERFACE_IS_PHONE){
        ((TrackSelectController*)segue.destinationViewController).delegate=self;
        ((TrackSelectController*)segue.destinationViewController).playMode=TRUE;
    }
}

#pragma mark Delegates

-(void) trackSelectClicked:(int)trackNumber :(BOOL)removeExisting
{
    [popupPlayView removeFromSuperview];
    popupPlayView=nil;
    if(trackNumber==0)
        selectedTrack=nil;
    else
        selectedTrack=[controller.settings.records objectForKey:[NSString stringWithFormat:@"Track0%d",trackNumber]];
    
    startPlaying = YES;
    auxSwiping = NO;
    
    [self takeScreenShot];
    [self loadChapter];

}
-(void) trackSelectClose{
    
    [popupPlayView removeFromSuperview];
    popupPlayView=nil;
    [self.navigationController popViewControllerAnimated:TRUE];

}

-(void) chapterClicked:(int)chapterIndex{

    [self takeScreenShot];
    
    auxSwiping=FALSE;
    [popupChapterView removeFromSuperview];
    popupChapterView=nil;
    [model.book pageAtIndex:chapterIndex];
    shouldAnimate=FALSE;

    [self loadChapter];
}

-(void) chapterClose
{
    auxSwiping = FALSE;
    [popupChapterView removeFromSuperview];
    popupChapterView=nil;
    if(timer)
        [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.003f target:self selector:@selector(update:) userInfo:nil repeats:YES];
    shouldAnimate=FALSE;
    [self loadChapter];
}

@end
