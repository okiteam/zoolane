//
//  AppDelegate.h
//  Zoolane
//
//  Created by Developer IMX on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "ASIHTTPRequestDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, FBSessionDelegate, ASIHTTPRequestDelegate>{
    BOOL hasJustEnteredForeground;
    Facebook* facebook;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Facebook* facebook;
@property(strong, nonatomic) ApplicationController *controller;
@property(assign, nonatomic) BOOL hasJustEnteredForeground;

@end
