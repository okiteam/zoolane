//
//  AudioPlayer.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "AudioPlayer.h"

#define TIMER_INTERVAL 0.05f

static const uint AudioPlayerStatePlaying = 0x01;
static const uint AudioPlayerStateStop = 0x01 << 1;
static const uint AudioPlayerStatePause = 0x01 << 2;
static const uint AudioPlayerStatePreloaded = 0x01 << 3;

@implementation AudioPlayer

//@synthesize delegate;
@synthesize delegate2;


-(id)init 
{
	self = [super init];
	
    if (self) 
    {
		soundVolume = 1.0;
        startTime = -1.f;
        playTime = 0.f;
        isPaused = NO;
	}
	return self;
}

-(void)setVolume:(float)v
{
    NSLog(@"AudioPlayer.m : -(void)setVolume:(float)v");
    NSLog(@"New Volume : %f", v);
	soundVolume = v;
	if (player)
    {
		[player setVolume:soundVolume];    
    }
}

-(float)volume
{
	return soundVolume;
}

-(void)preload:(NSURL*)url
{
	[self stop];
	if (!url)
    {
		return;
    }
	player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
	[player setDelegate:self];
	[player setVolume:soundVolume];
	[player prepareToPlay];
	state = AudioPlayerStatePreloaded;
}

-(NSInteger)preloadWithFullPath:(NSString*)path
{
	[self stop];

	if(path)
    {
		NSURL *url = [NSURL fileURLWithPath:path];
		
        if (url) 
        {
            player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
			[player setDelegate:self];
			[player setVolume:soundVolume];
            [player prepareToPlay];
			state = AudioPlayerStatePreloaded;
		}
	}
    else
    {
        NSLog(@"path : %@",path);
        NSLog(@"Wrong path!");
    }
    
    return 1;
}

-(void)playAtTime:(CGFloat)time
{
    startTime = time;
    [self play];
}

-(void)play
{
    if(state==AudioPlayerStateStop || state==AudioPlayerStatePause){
        [player setCurrentTime:0.0];
        [player prepareToPlay];
    }
    state = AudioPlayerStatePlaying;

    if(startTime <= 0.f)
    {
        [player play];
    }
    else
    {
        [self stopTimer];
        if(!playTimer)
            playTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(updatePlaying) userInfo:nil repeats:YES];
    }
}


-(void)updatePlaying
{
    if(!isPaused)
    {
        playTime += TIMER_INTERVAL;
        if(playTime >= startTime)
        {
            [player play];
            [self stopTimer];
        }
    }
}

-(void)stopTimer
{
    [playTimer invalidate];
    playTimer = nil;
    playTime = 0.f;
}

/*
-(void)playFile:(NSURL *)url
{
	[self stop];
	player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    [player setDelegate:self];
    [player setVolume:soundVolume];
	[player prepareToPlay];
	[player play];
	state = AudioPlayerStatePlaying;
}*/

-(void)stop
{
	[player stop];
	player = nil;
	state = AudioPlayerStateStop;
    [self stopTimer];
}

-(void)pause
{
    isPaused = YES;
	if (player.playing){
		[player pause];
		state = AudioPlayerStatePause;
	}
}

-(void)resume
{
    isPaused = NO;
	if (AudioPlayerStatePause == state)
    {
		[player play];
		state = AudioPlayerStatePlaying;
	}
}

#pragma mark  
#pragma mark AVAudioPlayerDelegate
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag 
{
	if(delegate2)
    {
		[delegate2 onSoundComplete:player.url];
        
        //don't care about the global var, we are sending the local one
        //[delegate onSoundComplete:player];
        
		//delegate2 = nil;
	}
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    NSLog(@"AudioPlayer.m : -(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player");
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withFlags:(NSUInteger)flags
{
    NSLog(@"AudioPlayer.m : -(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withFlags:(NSUInteger)flags");
}
#pragma mark  
#pragma mark System
-(void)dealloc
{
	[self stop];
}
@end
