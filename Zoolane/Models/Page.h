//
//  Page.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Page : NSObject 
{
	NSString* videoName;
	NSString* musicName;
	NSString* sfxName;
	NSString* voiceConversationName;
	NSString* voiceNarratorName;
	NSString* lrcConversationName;
	NSString* lrcNarratorName;
	NSString* backName;
	NSString* thumbName;
    NSString *textName;
    NSString *textContent;

    
	float symWidth;
}
@property (readwrite, retain) NSString* videoName;
@property (readwrite, retain) NSString* musicName;
@property (readwrite, retain) NSString* sfxName;
@property (readwrite, retain) NSString* voiceConversationName;
@property (readwrite, retain) NSString* voiceNarratorName;
@property (readwrite, retain) NSString* lrcConversationName;
@property (readwrite, retain) NSString* lrcNarratorName;
@property (readwrite, retain) NSString* backName;
@property (readwrite, retain) NSString* thumbName;
@property (readwrite, retain) NSString* textName;
@property (readwrite, retain) NSString* textContent;
@property (readwrite, retain) NSString* karaokeColor;
@property (readwrite, retain) NSString* karaokeNormalcolor;




@property (readwrite, assign) float symWidth;

-(id)init;

@end
