//
//  SettingsModel.m
//  imagealbum
//
//  Created by Developer IMX on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsModel.h"

@implementation SettingsModel
@synthesize records;

-(BOOL) loadConfig{
    NSLog(@"Loading settings");
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Records.plist"];
    if([[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        records = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        NSLog(@"Settings loaded %@",records);
    }
    else{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Records" ofType:@"plist"];
        records = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        NSLog(@"Settings loaded %@",records);
    }
    
    /*NSString *file = [[NSBundle mainBundle] pathForResource:@"Products" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:file];
    products=[dict objectForKey:@"Products"];
    defaultProduct=[products valueForKey:[dict objectForKey:@"DefaultProduct"]];
    NSLog(@"Default product %@",defaultProduct);
    NSLog(@"Products loaded %@",products);

    
    file = [[NSBundle mainBundle] pathForResource:@"Themes" ofType:@"plist"];
    dict = [NSDictionary dictionaryWithContentsOfFile:file];
    themes=[dict objectForKey:@"Themes"];
    
    NSLog(@"Themes loaded %@",themes);
*/
    
    return TRUE;
}

-(BOOL) saveConfig{
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Records.plist"];
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:records
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(@"Error : %@",error);
    }
    return TRUE;
    
}
@end
