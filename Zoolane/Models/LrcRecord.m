//
//  LrcRecord.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "LrcRecord.h"
#include <stdio.h>

@implementation LrcRecord
@synthesize timeBegin = _timeBegin;
@synthesize text = _text;
@synthesize timeEnd=_timeEnd;

static const char *WAIT_SYM = "^";

-(void)dealloc 
{
	_text = nil;
}

-(id)initWithTime:(float)timeBegin TimeEnd:(float)timeEnd andText:(NSString*)text;
{
	self =[super init];
	if (self) {
		_timeBegin = timeBegin;
        _timeEnd=timeEnd;
		_text = text;
        
	}
	return self;
}

	//--------------- class ----------------------------------------------------
+(LrcRecord*) readLrcFromFile:(FILE *)file
{
	while (!feof(file)) {
		uint s1, s2, s3;
        uint s11, s22, s33;
		char buf[256];
		char str[256];
		if (!fgets(buf, 255, file))
			continue;
        //[00:01.23]Lucy lives at 64 Zoo Lane.
//0:00.531-0:00.532- ♪
		if (sscanf( buf, "[%u:%u.%u]-[%u:%u.%u]%[^\r\n]", &s1, &s2, &s3,&s11,&s22,&s33, str) != 7)
			continue;
/*
		if (!strcmp(str, NEW_LINE))
			return [[LrcRecord alloc] initWithTime:(s1 * 60) + s2 + (float)s3 / 100.0f
										   andText:nil];
		else 
*/
		if (!strcmp(str, WAIT_SYM)){
            return [[LrcRecord alloc] initWithTime:(s1 * 60) + s2 + (float)s3 / 100.0f
        TimeEnd:(s11 * 60) + s22 + (float)s33 / 100.0f andText:nil];
        }
			
		else 
            return [[LrcRecord alloc] initWithTime:(s1 * 60) + s2 + (float)s3 / 100.0f
                                           TimeEnd:(s11 * 60) + s22 + (float)s33 / 100.0f andText:[NSString stringWithUTF8String:str]];
                                           
	}
	return nil;
}

+(NSArray/*LRCRecord*/*)readLrcsFromFile:(NSString*)path
{
	FILE *file = fopen([path UTF8String], "r");
	if (!file)
		return nil;
	NSMutableArray *lrcs = [[NSMutableArray alloc] init];
	LrcRecord *lrc;
	while ((lrc = [LrcRecord readLrcFromFile:file]))
		[lrcs addObject:lrc];
	fclose(file);
	return lrcs;
}

@end
