//
//  Book.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Page;

@interface Book : NSObject{
	NSMutableArray* pages;
	uint pageId;

	NSMutableArray *localizations;
}

-(id)init;

@property(readwrite, retain) NSMutableArray *pages;
@property(readwrite, retain) NSMutableArray *localizations;

-(Page*)firstPage;
-(Page*)page;

-(BOOL)nextPageExist;
-(BOOL)prevPageExist;
-(Page*)nextPage;
-(Page*)prevPage;
-(Page*)pageAtIndex:(uint)index;
-(uint)getCurrentIndex;
-(NSString*)getStringCurrentIndex;

@end
