//
//  Config.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "LanguageModel.h"
#import "SoftwareModel.h"
#import "iRate.h"

#import <QuartzCore/QuartzCore.h>

@implementation LanguageModel

static NSString *lang;
static NSBundle *bundle;

const char* const TRACK_0 = "track-1";
const char* const TRACK_1 = "track-2";
const char* const TRACK_2 = "track-3";

+(void)init
{
    NSString *languageOfTheUser = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if([languageOfTheUser isEqualToString:@"fr"])
    {
        [LanguageModel setLocale:@"fr"];
    }
    else if([languageOfTheUser isEqualToString:@"pt"])
    {
        [LanguageModel setLocale:@"br"];
    }
    else if([languageOfTheUser isEqualToString:@"es"])
    {
        [LanguageModel setLocale:@"es"];
    }
    else
    {
        [LanguageModel setLocale:@"en"];
    }
    
    
    if(UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom])
    {
        REC_BUTTON_POSITION_X = 436;
        PLY_BUTTON_POSITION_X = 524;
        CNT_BUTTON_POSITION_X = 480;
    }
    else
    {
        REC_BUTTON_POSITION_X = 191;
        PLY_BUTTON_POSITION_X = 258;
        CNT_BUTTON_POSITION_X = 224;
    }
}

+(UIColor*)UIColorFromRGB:(uint)color 
{
	return [UIColor colorWithRed:((float)((color & 0xff0000) >> 16)) / 255.0
						   green:((float)((color & 0x00ff00) >> 8)) / 255.0
							blue:((float)(color & 0x0000ff)) / 255.0 alpha:1.0];
}

+(UIColor*)systemColor 
{
	return [LanguageModel UIColorFromRGB:SYSTEM_COLOR];
}

	//---------- Low lang ------------------------------------------------------
+(void)setLocale:(NSString*)locale 
{
	lang = locale;
    if([lang isEqualToString:@"br"])
        locale = @"pt";
        
	NSString *path = [[NSBundle mainBundle] pathForResource:locale ofType:@"lproj"];
	bundle = [NSBundle bundleWithPath:path];
    [iRate sharedInstance].currentLanguage = locale;
}

+(NSString*)localeId 
{
	return lang;
}

+(NSString*)key2String:(NSString*)key 
{
	return [bundle localizedStringForKey:key value:nil table:nil];
}

	//---------- Common --------------------------------------------------------
+(void)addShowHideAnimation:(UIView*)view 
{
	CATransition *anim = [CATransition animation];
	[anim setDuration:0.5];
	[anim setType:kCATransitionFade];
	[anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];

	[[view layer] addAnimation:anim forKey:kCATransition];
}

@end
