//
//  Book.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "BookModel.h"
#import "Page.h"
#import "LanguageModel.h"

@implementation Book

@synthesize pages;
@synthesize localizations;

	//------------- Interface --------------------------------------------------
-(Page*)firstPage{
	pageId = 0;
	return [self page];
}

-(Page*)page{
	return [pages objectAtIndex:pageId];
}

-(BOOL)nextPageExist{
	return (pageId + 1 < [pages count]);
}

-(BOOL)prevPageExist{
	return !((int)pageId - 1 < 0);
}

-(Page*)nextPage{
	if ([self nextPageExist]){
		pageId++;
		return [self page];
	}
	return nil;
}

-(Page*)prevPage{
	if ([self prevPageExist]){
		pageId--;
		return [self page];
	}
	return  nil;
}

-(Page*)pageAtIndex:(uint)index{
	if (index < [pages count]){
		pageId = index;
		return [self page];
	}
	return  nil;
}

-(uint)getCurrentIndex{
	return pageId;
}

-(NSString*)getStringCurrentIndex
{
    NSString* stringIndex;
    if(pageId < 10)
        stringIndex = [NSString stringWithFormat:@"0%d", pageId];
    else
        stringIndex = [NSString stringWithFormat:@"%d", pageId];
    return stringIndex;
}


	//------------- System -----------------------------------------------------
-(id)init {
	self = [super init];
	if (self) {
		pages = [[NSMutableArray alloc] init];
		pageId = 0;
		localizations = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)dealloc{
}

@end
