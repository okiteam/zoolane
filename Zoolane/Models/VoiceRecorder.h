//
//  VoiceRecorder.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AVAudioRecorder;

@interface VoiceRecorder : NSObject{
//	NSURL *tmpRecordFile;
//	AVAudioRecorder *recorder;
    NSMutableArray* recorderArray;
    CGFloat recordTime;
    NSTimer* recordTimer;
}

//-(void)recordToFile:(NSString*)path;
//-(void)recordToFile:(NSString *)path startingAt:(NSTimeInterval)startTime during:(NSTimeInterval)duration;
-(void)addRecordToFile:(NSString *)path withTimingDic:(NSDictionary*)timingDic;
-(void)startRecording;

-(void)stop;
-(void)pause;
-(void)resume;

@end
