//
//  SoftwareModel.h
//  Zoolane
//
//  Created by Developer IMX on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BookModel.h"
#import "LanguageModel.h"
#import <AVFoundation/AVFoundation.h>
#import "AudioPlayer.h"
#import <MediaPlayer/MediaPlayer.h>
#import "VoiceRecorder.h"


#define INTERFACE_IS_PAD     ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) 
#define INTERFACE_IS_PHONE   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 

#define X_CENTER [UIScreen mainScreen].bounds.size.height * 0.5f
#define Y_CENTER [UIScreen mainScreen].bounds.size.width * 0.5f

#define DEFAULT_MODAL_TRANSITION UIModalTransitionStyleCoverVertical

#define ARROWS_TIME 3.0
#define PREVPAGE_TIME 3.0
#define SPLASH_TIME 3.0


#define KARAOKE_FONT @"Futura-Medium"
#define SYSTEM_FONT @"Futura-Medium"
#define PAGES_FONT @"Futura-Medium"

#define FONT_FUTURA @"Futura-Medium"
#define FONT_MAIANDRA @"Maiandra GD"
#define FONT_SERIFE @"MS Reference Sans Serif"

#define SYSTEM_COLOR 0x432a74
#define COLOR_LABEL 0x432a74
#define FONT_SIZE 25
#define FONT_SIZE_MEDIUM 24
#define FONT_SIZE_POPUP_VERSION 18
#define FONT_SIZE_SMALL 15.3

#define FONT_SIZE_IPHONE 14
#define FONT_SIZE_SMALL_IPHONE 8
#define FONT_SIZE_MEDIUM_IPHONE 12

#define RECORDSTART     @"start"
#define RECORDEND       @"end"
#define NARRATOR_ID     @"nar"
#define DIALOG_ID       @"dia"
#define WHOLESTORY_ID   @"all"
#define ISSTARTED       @"isStarted"
#define RECORDOBJ       @"recordObj"


@interface SoftwareModel : NSObject


@property (nonatomic,copy) NSString *pathDocuments;
@property (nonatomic,copy) NSString *pathSource;
@property (nonatomic,copy) NSString *pathBook;

@property (nonatomic,copy) NSString *pathTemp;

@property (nonatomic,strong) Book *book;

@property (nonatomic,strong) LanguageModel *language;

@property (readonly, strong) AudioPlayer *music;
//@property (readonly, strong) AudioPlayer* voice;
//@property (readonly, strong) AudioPlayer* voice2;
@property (readonly, strong) AudioPlayer* diaPLayer;
@property (readonly, strong) AudioPlayer* narPLayer;
@property (nonatomic, strong) NSMutableArray* audioPlayerArray;

@property (readonly, strong) AudioPlayer* sfx;

@property (nonatomic, strong) NSMutableArray* subtitles;

@property (readonly, strong) MPMoviePlayerController* moviePlayer;

@property (readonly, strong) VoiceRecorder* recorder;

@property bool showSplash;

@property (readonly, strong) NSMutableDictionary* allTimingNarDic;
@property (readonly, strong) NSMutableDictionary* allTimingDiaDic;

-(void)setTimingDictionary;

@end





@interface RecordTimingParserDelegate : NSObject<NSXMLParserDelegate>
{
    NSString* currentIndexKey;
}


@property (nonatomic, assign) NSMutableDictionary* allTimingNarDic;
@property (nonatomic, assign) NSMutableDictionary* allTimingDiaDic;

@end

