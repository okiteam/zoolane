//
//  VoiceRecorder.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "VoiceRecorder.h"
#import "SoftwareModel.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

#define TIMER_INTERVAL 0.05f

@implementation VoiceRecorder
static NSMutableDictionary *recSettings;

-(id)init {
	self = [super init];
	if (self){
		recSettings = [[NSMutableDictionary alloc] init];
		[recSettings setObject:[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
		[recSettings setObject:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
		[recSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
		[recSettings setObject:[NSNumber numberWithInt:1280] forKey:AVEncoderBitRateKey];
		[recSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
		[recSettings setObject:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
	}
	return self;
}

-(void)dealloc {
	[self stop];
	recSettings = nil;
}

#pragma -
#pragma - Record methods

-(void)addRecordToFile:(NSString *)path withTimingDic:(NSDictionary*)timingDic
{
    [recordTimer invalidate];
    recordTimer = nil;
    if(!recorderArray)
        recorderArray = [[NSMutableArray alloc] init];
    
 	NSURL* recordFilePath = [NSURL fileURLWithPath:path];
	NSError *err;
    AVAudioRecorder* rec = nil;
	if (!(rec = [[AVAudioRecorder alloc] initWithURL:recordFilePath settings:recSettings error:&err]))
		[NSException raise:@"Recorder exception" format:@"creating recorder failed"];
    
	[rec setMeteringEnabled:YES];
    NSMutableDictionary* recordDic = nil;
    if(timingDic)
        recordDic = [NSMutableDictionary dictionaryWithDictionary:timingDic];
    else
        recordDic = [[NSMutableDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.f], [NSNumber numberWithFloat:9999.f], nil]
                                                         forKeys:[NSArray arrayWithObjects:RECORDSTART, RECORDEND, nil]];
    [recordDic setObject:rec forKey:RECORDOBJ];
    BOOL isStarted = NO;
    [recordDic setObject:[NSNumber numberWithBool:isStarted] forKey:ISSTARTED];
    
    [recorderArray addObject:recordDic];
}


-(void)startRecording
{
    recorderArray = (NSMutableArray*)[[recorderArray reverseObjectEnumerator] allObjects];
    recordTime = 0.f;
    if(!recordTimer)
        recordTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(updateRecording) userInfo:nil repeats:YES];
}


-(void)updateRecording
{
    if([recorderArray count] < 1)
    {
        [self stop];
        return;
    }
    
    recordTime += TIMER_INTERVAL;
    NSMutableDictionary* currentRecordDic = [recorderArray lastObject];
    BOOL isStarted = [[currentRecordDic objectForKey:ISSTARTED] boolValue];
    if(!isStarted)
    {
        CGFloat start = [[currentRecordDic objectForKey:RECORDSTART] floatValue];
        // Launch record ?
        if(recordTime > start)
        {
            AVAudioRecorder* currentRec = [currentRecordDic objectForKey:RECORDOBJ];
            if([currentRec record])
            {
                NSLog(@"record start at : %f", recordTime);
                [currentRecordDic setObject:[NSNumber numberWithBool:YES] forKey:ISSTARTED];
            }
        }
    }
    else
    {
        CGFloat end = [[currentRecordDic objectForKey:RECORDEND] floatValue];
        // End record ?
        if(recordTime > end)
        {
            AVAudioRecorder* currentRec = [currentRecordDic objectForKey:RECORDOBJ];
            [currentRec stop];
            NSLog(@"record end at : %f", recordTime);
            [recorderArray removeLastObject];
        }
    }
}


-(void)stop
{
    recordTime = 0.f;
    [recordTimer invalidate];
    recordTimer = nil;
    
    if(recorderArray)
    {
        for (NSMutableDictionary* currentRecordDic in recorderArray) {
             AVAudioRecorder* rec = [currentRecordDic objectForKey:RECORDOBJ];
            [rec stop];
        }
        [recorderArray removeAllObjects];
        recorderArray = nil;
    }
}


// Methode pas utilisees normalement
-(void)pause{
//	[recorder pause];
    NSLog(@"la methode pause nexiste plus");
//    for (AVAudioRecorder* rec in recorderArray) {
  //      [rec pause];
    //}
}

-(void)resume{
//	[recorder record];
    NSLog(@"la methode resume nexiste plus");
}


@end
