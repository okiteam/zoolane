//
//  LrcRecord.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LrcRecord;

@interface LrcRecord : NSObject
{
	
}

@property (readonly, assign) float timeBegin;
@property (readonly, assign) float timeEnd;

@property (readonly, retain) NSString *text;

-(id)initWithTime:(float)timeBegin TimeEnd:(float)timeEnd andText:(NSString*)text;

+(NSMutableArray/*LRCRecord*/*)readLrcsFromFile:(NSString*)path;

@end
