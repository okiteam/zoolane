//
//  SoftwareModel.m
//  Zoolane
//
//  Created by Developer IMX on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SoftwareModel.h"
#import "BookModel.h"
#import "BookXMLParser.h"


@implementation SoftwareModel

@synthesize pathSource, pathTemp, pathDocuments,pathBook;
@synthesize book;
@synthesize language;
@synthesize music, sfx;//voice,voice2,sfx;
@synthesize diaPLayer;
@synthesize narPLayer;
@synthesize audioPlayerArray;
@synthesize moviePlayer;
@synthesize subtitles;
@synthesize recorder;
@synthesize showSplash;
@synthesize allTimingNarDic;
@synthesize allTimingDiaDic;

-(id) init{
    
    pathDocuments = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    pathSource = [[NSBundle mainBundle] resourcePath];
    pathTemp = NSTemporaryDirectory();
    pathBook = [pathSource stringByAppendingPathComponent:@"Book"];
    [[NSFileManager defaultManager] removeItemAtPath:pathTemp error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:pathTemp withIntermediateDirectories:FALSE attributes:nil error:nil];
    
    
    NSString *path = [pathSource stringByAppendingPathComponent:@"Book/book.xml"];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]];
    BookXMLParser *delegete = [[BookXMLParser alloc] init];
    [parser setDelegate:delegete];
    [parser parse];
    book = delegete.book;
    
    [LanguageModel init];
    
    music = [[AudioPlayer alloc]init];
    sfx = [[AudioPlayer alloc]init];
//    voice = [[AudioPlayer alloc]init];
  //  voice2 = [[AudioPlayer alloc]init];
    diaPLayer = [[AudioPlayer alloc]init];
    narPLayer = [[AudioPlayer alloc]init];
    audioPlayerArray = [[NSMutableArray alloc] init];
    moviePlayer = [[MPMoviePlayerController alloc]init];
    
    subtitles = [[NSMutableArray alloc]init];
    
    showSplash = true;
    
    
    // Timing -> cest deja fait quand on arrive sur la home
   // [self setTimingDictionary];
    return self;
}

-(void)setTimingDictionary
{
    // TO DO : YANN mettre ca quand on change de langue
    
    NSString *path = [pathSource stringByAppendingPathComponent:@"Book/recordTiming"];
    path = [path stringByAppendingPathComponent:[LanguageModel localeId]];
    path = [path stringByAppendingPathComponent:@"record_timing.xml"];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]];
    
    if(!allTimingNarDic)
        allTimingNarDic = [[NSMutableDictionary alloc] init];
    else
        [allTimingNarDic removeAllObjects];
    if(!allTimingDiaDic)
        allTimingDiaDic = [[NSMutableDictionary alloc] init];
    else
        [allTimingDiaDic removeAllObjects];
    RecordTimingParserDelegate *timingDelegate = [[RecordTimingParserDelegate alloc] init];
    parser.delegate = timingDelegate;
    timingDelegate.allTimingNarDic = allTimingNarDic;
    timingDelegate.allTimingDiaDic = allTimingDiaDic;
    [parser parse];

}

@end



@implementation RecordTimingParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if(!currentIndexKey)
        currentIndexKey = [[NSString alloc] init];
    
	if ([elementName isEqualToString:@"timing"]) {
        currentIndexKey = [attributeDict objectForKey:@"indexchapter"];
	}
    
	else if([elementName isEqualToString:NARRATOR_ID]) {
        
        //On regarde si il faut créer le timingArray
        NSMutableArray* currentTimingArray = [self.allTimingNarDic objectForKey:currentIndexKey];
        if(!currentTimingArray)
        {
            currentTimingArray = [[NSMutableArray alloc] init];
            [self.allTimingNarDic setObject:currentTimingArray forKey:currentIndexKey];
        }
        
        NSArray* keyArray = [NSArray arrayWithObjects:RECORDSTART, RECORDEND, nil];
        NSArray* objArray = [NSArray arrayWithObjects:
                             [attributeDict objectForKey:RECORDSTART],
                             [attributeDict objectForKey:RECORDEND], nil];
        
        NSDictionary* timingDictionary = [[NSDictionary alloc] initWithObjects:objArray forKeys:keyArray];
        [currentTimingArray addObject:timingDictionary];
    }
    
	else if ([elementName isEqualToString:DIALOG_ID]) {
        
        //On regarde si il faut créer le timingArray
        NSMutableArray* currentTimingArray = [self.allTimingDiaDic objectForKey:currentIndexKey];
        if(!currentTimingArray)
        {
            currentTimingArray = [[NSMutableArray alloc] init];
            [self.allTimingDiaDic setObject:currentTimingArray forKey:currentIndexKey];
        }
        
        NSArray* keyArray = [NSArray arrayWithObjects:RECORDSTART, RECORDEND, nil];
        NSArray* objArray = [NSArray arrayWithObjects:
                             [attributeDict objectForKey:RECORDSTART],
                             [attributeDict objectForKey:RECORDEND], nil];
        
        NSDictionary* timingDictionary = [[NSDictionary alloc] initWithObjects:objArray forKeys:keyArray];
        [currentTimingArray addObject:timingDictionary];
    }
}


@end
