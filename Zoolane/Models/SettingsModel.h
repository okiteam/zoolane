//
//  SettingsModel.h
//  imagealbum
//
//  Created by Developer IMX on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsModel : NSObject
{
    
}
 
@property (nonatomic,strong) NSMutableDictionary *records;


-(BOOL) loadConfig;

-(BOOL) saveConfig;

@end
