//
//  BookXMLParser.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "BookXMLParser.h"
#import "BookModel.h"
#import "Page.h"
#import "SoftwareModel.h"

@implementation BookXMLParser

@synthesize book;

-(id)init {
	self = [super init];
	if (self) {
		book = [[Book alloc] init];
	}
	return self;
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
		// read localization's names
	if ([elementName isEqualToString:@"language"]) {
		[book.localizations addObject:[attributeDict objectForKey:@"name"]];
	}

	if ([elementName isEqualToString:@"page"]) {
		Page *page = [[Page alloc] init];

		page.videoName =[NSString stringWithFormat:@"video/%@.mp4",[attributeDict objectForKey:@"video"]];
		page.musicName = [NSString stringWithFormat:@"audio/music/%@.mp3",[attributeDict objectForKey:@"music"]];
                          
		page.sfxName = [NSString stringWithFormat:@"audio/sfx/%@.mp3",[attributeDict objectForKey:@"sfx"]];
		page.voiceConversationName = [NSString stringWithFormat:@"audio/voice/LANGCODE/%@.mp3",[attributeDict objectForKey:@"voice_conversation"]];
		page.voiceNarratorName = [NSString stringWithFormat:@"audio/voice/LANGCODE/%@.mp3",[attributeDict objectForKey:@"voice_narrator"] ];
        
		page.lrcConversationName = [NSString stringWithFormat:@"lrc/LANGCODE/%@_DEVICE.lrc",[attributeDict objectForKey:@"lrc_conversation"] ];
        page.lrcNarratorName = [NSString stringWithFormat:@"lrc/LANGCODE/%@_DEVICE.lrc",[attributeDict objectForKey:@"lrc_narrator"] ];
        
      //   page.textName = [NSString stringWithFormat:@"lrc/LANGCODE/%@.txt",[attributeDict objectForKey:@"text"] ];

		page.backName = [NSString stringWithFormat:@"back/%@.png",[attributeDict objectForKey:@"back"] ];
        
        //[attributeDict objectForKey:@"back"];
		page.thumbName = [NSString stringWithFormat:@"thumb/%@.png",[attributeDict objectForKey:@"thumb"] ];
        
        page.karaokeNormalcolor=[attributeDict objectForKey:@"karaoke_normal"];
        page.karaokeColor=[attributeDict objectForKey:@"karaoke_color"];

        
		if ([attributeDict objectForKey:@"sym_width"])
			page.symWidth = [[attributeDict objectForKey:@"sym_width"] floatValue];

		[book.pages addObject:page];
	}
}

-(UIColor*)colorWithHexString:(NSString*)hex  
{  
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
    
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return [UIColor grayColor];  
    
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
    
    if ([cString length] != 6) return  [UIColor grayColor];  
    
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
    
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
    
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
    
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
    
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
} 
@end
