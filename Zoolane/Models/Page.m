//
//  Page.m
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

//#import "LogUtils.h"
#import "Page.h"

@implementation Page

@synthesize videoName;
@synthesize musicName;
@synthesize sfxName;
@synthesize voiceConversationName;
@synthesize voiceNarratorName;
@synthesize lrcConversationName;
@synthesize lrcNarratorName;
@synthesize backName;
@synthesize thumbName;
@synthesize textName;
@synthesize textContent;
@synthesize karaokeColor;
@synthesize karaokeNormalcolor;
@synthesize symWidth;

-(id)init {
	self = [super init];
	if (self)
		symWidth = -1;
	return self;
}

@end
