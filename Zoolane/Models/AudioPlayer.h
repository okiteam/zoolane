//
//  AudioPlayer.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>

@protocol AudioPlayerDelegate

-(void) onSoundComplete:(NSURL*)url;

@end

@class AVAudioPlayer;

@interface AudioPlayer : NSObject<AVAudioPlayerDelegate>
{
    @public
	AVAudioPlayer *player;
	uint state;
	float soundVolume;
    
    BOOL isPaused;
    CGFloat startTime;
    NSTimer* playTimer;
    CGFloat playTime;
}

-(void)preload:(NSURL*)url;
-(NSInteger)preloadWithFullPath:(NSString*)path; 
-(void)playAtTime:(CGFloat)time;
-(void)play;
-(void)stop;
-(void)pause;
-(void)resume;
-(float)volume;
-(void)setVolume:(float)v;

@property (readwrite, retain) NSObject<AudioPlayerDelegate> *delegate2;

@end
