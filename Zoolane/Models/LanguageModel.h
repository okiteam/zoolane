//
//  Config.h
//  ZooLane
//
// Created by Alexandre Chatterjee on 21.05.12.
// Copyright (c) 2012 Familia Games. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const char* const TRACK_0;
extern const char* const TRACK_1;
extern const char* const TRACK_2;


NSUInteger REC_BUTTON_POSITION_X;
NSUInteger PLY_BUTTON_POSITION_X;
NSUInteger CNT_BUTTON_POSITION_X;

@interface LanguageModel : NSObject
+(void)init;
//+(NSString*)systemFont;
//+(NSString*)karaokeFont;
//+(NSString*)pagesFont;
+(UIColor*)systemColor;
+(UIColor*)UIColorFromRGB:(uint)color;

+(void)setLocale:(NSString*)locale;
+(NSString*)localeId;
+(NSString*)key2String:(NSString*)key;

+(void)addShowHideAnimation:(UIView*)view;
@end
